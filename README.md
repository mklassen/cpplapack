C++ wrappers for calling LAPACK functions

* Supports passing `std::vector` and `size_t` length instead of pointers and `int`
* Support pointers and `int`
* All passing by reference
* Error handling and throwing `cpplapack::lapack_error` for error conditions
