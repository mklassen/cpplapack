#define DATA_TYPE 0
#include "test_functions.hpp"

#define DATA_TYPE 1
#include "test_functions.hpp"

#define DATA_TYPE 2
#include "test_functions.hpp"

#define DATA_TYPE 3
#include "test_functions.hpp"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wc++11-extensions"
#pragma clang diagnostic ignored "-Wvariadic-macros"
#include "gtest/gtest.h"
#pragma clang diagnostic pop

 int main(int argc, char *argv[])
{
   ::testing::InitGoogleTest(&argc, argv);
   return RUN_ALL_TESTS();
}
