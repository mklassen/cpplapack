#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wc++11-extensions"
#pragma clang diagnostic ignored "-Wvariadic-macros"
#include "gtest/gtest.h"
#pragma clang diagnostic pop

#include "cpplapack.h"

namespace {

   template<typename T> void makeData(std::vector<T> &data)
   {
      for (size_t i=0; i < data.size(); i++)
         data[i] = T(i);
   }

   void makeData(std::vector< std::complex<double> > &data)
   {
      for (size_t i=0; i < data.size(); i++)
         data[i] = std::complex<double> (i+1.0, i+2.0);
   }

   void makeData(std::vector< std::complex<float> > &data)
   {
      for (size_t i=0; i < data.size(); i++)
         data[i] = std::complex<float> (i+1.0f, i+2.0f);
   }

   template<typename T> void checkTransposeN()
   {
      size_t m(5);
      size_t n(4);
      size_t sPoints(m*n);

      std::vector<T> dData(sPoints);
      std::vector<T> dResult(sPoints);
      makedata(dData);

      cpplapack::transpose('N', m, n, dData, m, dResult, m);

      for (size_t j=0; j < dData.size(); j++)
      {
         ASSERT_EQ(dData[j], dResult[j]);
      }
   }

   template<typename T> void checkTransposeT()
   {

      size_t m(5);
      size_t n(4);
      size_t sPoints(m*n);

      std::vector<T> dData(sPoints);
      std::vector<T> dResult(sPoints);
      makedata(dData);

      cpplapack::transpose('T', m, n, dData, n, dResult, m);

      for (size_t j=0; j < m; j++)
      {
         for (size_t i=0; i < n; i++)
         {
            ASSERT_EQ(dData[i + j*n], dResult[j + i*m]);
         }
      }
   }

   template<typename T> void checkTransposeR()
   {

      size_t m(5);
      size_t n(4);
      size_t sPoints(m*n);

      std::vector<T> dData(sPoints);
      std::vector<T> dResult(sPoints);
      makedata(dData);

      cpplapack::transpose('R', m, n, dData, m, dResult, m);

      for (size_t j=0; j < dData.size(); j++)
      {
         ASSERT_EQ(dData[j], std::conj(dResult[j]));
      }
   }

   template<typename T> void checkTransposeC()
   {

      size_t m(5);
      size_t n(4);
      size_t sPoints(m*n);

      std::vector<T> dData(sPoints);
      std::vector<T> dResult(sPoints);
      makedata(dData);

      cpplapack::transpose('C', m, n, dData, n, dResult, m);

      for (size_t j=0; j < m; j++)
      {
         for (size_t i=0; i < n; i++)
         {
            ASSERT_EQ(dData[i + j*n], std::conj(dResult[j + i*m]));
         }
      }
   }

   TEST(CppLapackTest, transpose_N_double)
   {
      checktransposeN<double>();
   }
   TEST(CppLapackTest, transpose_T_double)
   {
      checktransposeT<double>();
   }
   TEST(CppLapackTest, transpose_R_double)
   {
      checktransposeR<double>();
   }
   TEST(CppLapackTest, transpose_C_double)
   {
      checktransposeC<double>();
   }

   TEST(CppLapackTest, transpose_N_complexdouble)
   {
      checktransposeN< std::complex<double> >();
   }
   TEST(CppLapackTest, transpose_T_complexdouble)
   {
      checktransposeT< std::complex<double> >();
   }
   TEST(CppLapackTest, transpose_R_complexdouble)
   {
      checktransposeR< std::complex<double> >();
   }
   TEST(CppLapackTest, transpose_C_complexdouble)
   {
      checktransposeC< std::complex<double> >();
   }

   TEST(CppLapackTest, transpose_N_float)
   {
      checktransposeN<float>();
   }
   TEST(CppLapackTest, transpose_T_float)
   {
      checktransposeT<float>();
   }
   TEST(CppLapackTest, transpose_R_float)
   {
      checktransposeR<float>();
   }
   TEST(CppLapackTest, transpose_C_float)
   {
      checktransposeC<float>();
   }

   TEST(CppLapackTest, transpose_N_complexfloat)
   {
      checktransposeN< std::complex<float> >();
   }
   TEST(CppLapackTest, transpose_T_complexfloat)
   {
      checktransposeT< std::complex<float> >();
   }
   TEST(CppLapackTest, transpose_R_complexfloat)
   {
      checktransposeR< std::complex<float> >();
   }
   TEST(CppLapackTest, transpose_C_complexfloat)
   {
      checktransposeC< std::complex<float> >();
   }

}
