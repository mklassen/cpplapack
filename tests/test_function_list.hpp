   axpy(n, alpha, x, incx, y, incy);
#if DATA_TYPE >= 2
   dot(n, x, incx, y, incy);
   lamch('N',alpha);
   labad(alpha,alpha);
   gesvd('N','N',m,n,a,lda,rwork,a,lda,work,lwork,a,lda);
   gesdd('N',m,n,a,lda,rwork,a,lda,a,lda,work,lwork,iwork);
   gelsy(m,n,nrhs,a,lda,b,ldb,iwork,rcond,n,work,lwork);
   gelss(m,n,nrhs,a,lda,b,ldb,rwork,rcond,n,work,lwork);
   gelsd(m,n,nrhs,a,lda,b,ldb,rwork,rcond,n,work,lwork,iwork);
   geqp3(m,n,a,lda,iwork,a,work,lwork);
#else
   dotc(n, x, incx, y, incy);
   dotu(n, x, incx, y, incy);
   hemv('N',n,alpha,a,lda,x,incx,beta,y,incy);
   hemm('N','N',m,n,alpha,a,lda,b,ldb,beta,c,ldc);
   herk('N','N',n,k,alpha,a,lda,beta,c,ldc);
   gesvd('N','N',m,n,a,lda,rwork,a,lda,a,lda,work,lwork,rwork);
   gesdd('N',m,n,a,lda,rwork,a,lda,a,lda,work,lwork,rwork,iwork);
   gelsy(m,n,nrhs,a,lda,b,ldb,iwork,rcond,n,work,lwork,rwork);
   gelss(m,n,nrhs,a,lda,b,ldb,rwork,rcond,n,work,lwork,rwork);
   gelsd(m,n,nrhs,a,lda,b,ldb,rwork,rcond,n,work,lwork,rwork,iwork);
   heevr('N','N','N',n,a,lda,rcond,rcond,n,n,rcond,m,rwork,a,lda,iwork,work,lwork,rwork,n,iwork,n);
   heevd('N','N',n,a,lda,rwork,work,lwork,rwork,n,iwork,n);
   heev('N','N',n,a,lda,rwork,work,lwork,rwork);
   geqp3(m,n,a,lda,iwork,a,work,lwork,rwork);
   unmlq('N','N',m,n,k,a,lda,a,c,ldc,work,lwork);
   unmqr('N','N',m,n,k,a,lda,a,c,ldc,work,lwork);
   unmrz('N','N',m,n,k,k,a,lda,a,c,ldc,work,lwork);
   hetri('N',n,a,lda,iwork,work);
   hetrf('N',n,a,lda,iwork,work,lwork);
#endif
   nrm2(n, x, incx);
   scal(n, alpha, x,incx);

   gemv('N',m,n,alpha,a,lda,x,incx,beta,y,incy);
   gemm('N','N',m,n,k,alpha,a,lda,b,ldb,beta,c,ldc);
   trmm('N','N','N','N',m,n,alpha,a,lda,b,ldb);
   syrk('N','N',n,k,alpha,a,lda,beta,c,ldc);

   ilaenv(n,"A","B",n,n,n,n);
   lange('N',m,n,a,lda,rwork);

   gels('N',m,n,nrhs,a,lda,b,ldb,work,lwork);
   gesv(m,n,a,lda,iwork,b,ldb);
   posv('N',n,nrhs,a,lda,b,ldb);

   geqrf(m,n,a,lda,work,work,lwork);
   gelqf(m,n,a,lda,work,work,lwork);

   trtrs('N','N','N',n,nrhs,a,lda,b,ldb);
   trsm('N','N','N','N',m,n,alpha,a,lda,b,ldb);
   tzrzf(m,n,a,lda,a,work,lwork);
   getrf(m,n,a,lda,iwork);
   getri(m,a,lda,iwork,work,lwork);
   getrs('N',n,nrhs,a,lda,iwork,b,ldb);
   potri('N',n,a,lda);
   potrf('N',n,a,lda);
   lascl('N',n,n,rcond,rcond,m,n,a,lda);
   laic1(1,n,a,rcond,a,alpha,rcond,alpha,alpha);
