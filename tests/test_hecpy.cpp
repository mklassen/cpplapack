#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wc++11-extensions"
#pragma clang diagnostic ignored "-Wvariadic-macros"
#include "gtest/gtest.h"
#pragma clang diagnostic pop

#include "cpplapack.h"

namespace {

   TEST(CppLapackTest, hecpy_double)
   {
      size_t sDim(5);
      size_t sPoints(sDim*sDim);

      std::vector<std::complex<double> > dData(sPoints);
      std::vector<std::complex<double> > dData2(sPoints);

      for (size_t i=0; i < dData.size(); i++)
         dData[i] = std::complex<double> (i+1.0, i+2.0);

      cpplapack::hecpy('U', sDim, dData, dData2);

      for (size_t j=0; j < sDim; j++)
      {
         for (size_t i=0; i <= j; i++)
         {
            ASSERT_EQ(dData[i + j*sDim], dData2[i + j*sDim]);
         }
         for (size_t i=j+1; i < sDim; i++)
         {
            ASSERT_EQ(dData[j + i*sDim], std::conj(dData2[i + j*sDim]));
         }
      }

      for (size_t i=0; i < sPoints; i++)
         dData[i] = std::complex<double> (i+1.0, i+2.0);

      cpplapack::hecpy('L', sDim, dData, dData2);

      for (size_t j=0; j < sDim; j++)
      {
         for (size_t i=j+1; i < sDim; i++)
         {
            ASSERT_EQ(dData[i + j*sDim], dData2[i + j*sDim]);
         }
         for (size_t i=0; i < j; i++)
         {
            ASSERT_EQ(dData[j + i*sDim], std::conj(dData2[i + j*sDim]));
         }
      }
   }

   TEST(CppLapackTest, hecpy_float)
   {
      size_t sDim(5);
      size_t sPoints(sDim*sDim);

      std::vector<std::complex<float> > dData(sPoints);
      std::vector<std::complex<float> > dData2(sPoints);

      for (size_t i=0; i < dData.size(); i++)
         dData[i] = std::complex<float> (i+1.0, i+2.0);

      cpplapack::hecpy('U', sDim, dData, dData2);

      for (size_t j=0; j < sDim; j++)
      {
         for (size_t i=0; i <= j; i++)
         {
            ASSERT_EQ(dData[i + j*sDim], dData2[i + j*sDim]);
         }
         for (size_t i=j+1; i < sDim; i++)
         {
            ASSERT_EQ(dData[j + i*sDim], std::conj(dData2[i + j*sDim]));
         }
      }

      for (size_t i=0; i < dData.size(); i++)
         dData[i] = std::complex<float> (i+1.0, i+2.0);

      cpplapack::hecpy('L', sDim, dData, dData2);

      for (size_t j=0; j < sDim; j++)
      {
         for (size_t i=j+1; i < sDim; i++)
         {
            ASSERT_EQ(dData[i + j*sDim], dData2[i + j*sDim]);
         }
         for (size_t i=0; i < j; i++)
         {
            ASSERT_EQ(dData[j + i*sDim], std::conj(dData2[i + j*sDim]));
         }
      }
   }
}
