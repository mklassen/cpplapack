#include "cpplapack.h"

#define MANGLE_TEST(TYPE, PREFIX) compile_test_ ## PREFIX ## TYPE
#define TEST_FUNCTION(TYPE) MANGLE_TEST(TYPE, A)
#define TEST_FUNCTION2(TYPE) MANGLE_TEST(TYPE, B)

#if DATA_TYPE == 0
   #define MY_TYPE complex_double
#elif DATA_TYPE == 1
   #define MY_TYPE complex_float
#elif DATA_TYPE == 2
   #define MY_TYPE double
#elif DATA_TYPE == 3
  #define MY_TYPE float
#endif

using namespace cpplapack;

void TEST_FUNCTION(MY_TYPE)()
{
   std::vector<MY_TYPE> a;
   std::vector<MY_TYPE> b;
   std::vector<MY_TYPE> c;

   std::vector<MY_TYPE> x;
   std::vector<MY_TYPE> y;

   std::vector<MY_TYPE> work;

   size_t n(0);
   size_t m(0);
   size_t k(0);

   size_t lda(0);
   size_t ldb(0);
   size_t ldc(0);

   size_t incx(0);
   size_t incy(0);

   size_t nrhs(0);

   size_t lwork(0);

   MY_TYPE alpha(0);
   MY_TYPE beta(0);

#if DATA_TYPE == 0 || DATA_TYPE == 2
   std::vector<double> rwork;
   double rcond(0);
#elif DATA_TYPE == 1 || DATA_TYPE == 3
   std::vector<float> rwork;
   float rcond(0);
#endif

   std::vector<int> iwork;

#include "test_function_list.hpp"
}

void TEST_FUNCTION2(MY_TYPE)()
{
   MY_TYPE *a(NULL);
   MY_TYPE *b(NULL);
   MY_TYPE *c(NULL);

   MY_TYPE *x(NULL);
   MY_TYPE *y(NULL);

   MY_TYPE *work(NULL);

   int n(0);
   int m(0);
   int k(0);

   int lda(0);
   int ldb(0);
   int ldc(0);

   int incx(0);
   int incy(0);

   int nrhs(0);

   int lwork(0);

   MY_TYPE alpha(0);
   MY_TYPE beta(0);

#if DATA_TYPE == 0 || DATA_TYPE == 2
   double *rwork(NULL);
   double rcond(0);
#elif DATA_TYPE == 1 || DATA_TYPE == 3
   float *rwork(NULL);
   float rcond(0);
#endif

   int *iwork(NULL);

#include "test_function_list.hpp"
}

#undef DATA_TYPE
#undef MY_TYPE
#undef MANGLE_TEST
#undef TEST_FUNCTION
#undef TEST_FUNCTION2

