cmake_minimum_required(VERSION 2.6.4)
project(cpplapack)
enable_testing()
add_subdirectory(tests)
