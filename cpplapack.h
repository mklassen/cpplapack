//
//  C++ interface to LAPACK functions
//  lapack.h
//
//  Created by Martyn Klassen on 12-03-06.
//  Copyright (c) 2012 Robarts Research Institute. All rights reserved.
//

#ifndef cpplapack_h
#define cpplapack_h

#include <complex>
#include <exception>
#include <stdexcept>
#include <sstream>
#include <vector>
#include <cassert>

#ifdef __INTEL_COMPILER
#define MKL 1
#else
#define MKL 0
#endif

#define LAPACK(function) function##_

#if defined(ICEIDEAFUNCTORS_EXPORTS) & defined(PARC_MODULE_NAME)

#define nullptr NULL
#define MKL 0
#if BUILD_PLATFORM_LINUX
#undef LAPACK
#define LAPACK(function) function
#endif

#endif


#ifndef __FUNCTION__
#ifdef __func__
#define __FUNCTION__ __func__
#else
#define __FUNCTION__ "Compiler does not support __FUNCTION__"
#endif
#endif

#if MKL

#define MKL_Complex8 std::complex<float>
#define MKL_Complex16 std::complex<double>

#include "mkl.h"

#define SOURCE(name) LAPACK(name)

#else

#define SOURCE(name) cpplapack_private::LAPACK(name)

#endif


namespace cpplapack {

   class lapack_error : public std::runtime_error
   {
   public:
      explicit lapack_error(const std::string &str, const int &info)
         : std::runtime_error(str)
         , m_iError(info)
         {}

      int error() { return m_iError; }
   private:
      int m_iError;
   };

   typedef std::complex<double> complex_double;
   typedef std::complex<float>  complex_float;

   namespace cpplapack_private {

#define LAPACK_ERROR(name, msg, info) if (info) { \
            std::stringstream ss; \
            if ((info) < 0) \
               ss << (name) << " argument " << -(info) << " is invalid."; \
            else if (info) \
               ss << (name) << ": " << (msg) << ": " << (info); \
            throw lapack_error(ss.str(), (info)); \
         }

#if defined(__STDC_VERSION__) && __STDC_VERSION__ >= 199901L
#include <complex.h>
typedef float _Complex complex_float_c;
typedef double _Complex complex_double_c;
#else
      typedef struct
      {
         float re, im;
      public:
         operator complex_float () const
         {
            return *(reinterpret_cast<complex_float const *const> (this));
         }
      } complex_float_c;
      typedef struct
      {
         double re, im;
      public:
         operator complex_double () const
         {
            return *(reinterpret_cast<complex_double const *const> (this));
         }
      } complex_double_c;
#endif

      extern "C" {

         extern complex_double_c LAPACK(zdotu)(const int *n, const complex_double *dx, const int *incx, const complex_double *dy, const int *incy);
         extern complex_double_c LAPACK(zdotc)(const int *n, const complex_double *dx, const int *incx, const complex_double *dy, const int *incy);
         extern complex_float_c LAPACK(cdotu)(const int *n, const complex_float *dx, const int *incx, const complex_float *dy, const int *incy);
         extern complex_float_c LAPACK(cdotc)(const int *n, const complex_float *dx, const int *incx, const complex_float *dy, const int *incy);

         // BLAS 1
         extern void LAPACK(zaxpy)(const int *n,
                                   const complex_double *alpha, const complex_double *x, const int *incx,
                                   complex_double *y, const int *incy);
         extern void LAPACK(caxpy)(const int *n,
                                   const complex_float *alpha, const complex_float *x, const int *incx,
                                   complex_float *y, const int *incy);
         extern void LAPACK(daxpy)(const int *n,
                                   const double *alpha, const double *x, const int *incx,
                                   double *y, const int *incy);
         extern void LAPACK(saxpy)(const int *n,
                                   const float *alpha, const float *x, const int *incx,
                                   float *y, const int *incy);

         extern double LAPACK(dnrm2)(const int *n, const double *x, const int *incx);
         extern double LAPACK(dznrm2)(const int *n, const complex_double *x, const int *incx);
         extern float LAPACK(snrm2)(const int *n, const float *x, const int *incx);
         extern float LAPACK(scnrm2)(const int *n, const complex_float *x, const int *incx);

         extern void LAPACK(zscal)(const int *N,  const complex_double *ALPHA, complex_double *X, const int *INCX);
         extern void LAPACK(cscal)(const int *N,  const complex_float *ALPHA, complex_float *X, const int *INCX);
         extern void LAPACK(dscal)(const int *N,  const double *ALPHA, double *X, const int *INCX);
         extern void LAPACK(sscal)(const int *N,  const float *ALPHA, float *X, const int *INCX);

         extern double LAPACK(ddot)(const int *n, const double *dx, const int *incx, const double *dy, const int *incy);
         extern float LAPACK(sdot)(const int *n, const float *dx, const int *incx, const float *dy, const int *incy);

         // BLAS 2
         extern void LAPACK(zgemv)(const char *joba, const int *m, const int *n,
                            const complex_double *alpha, const complex_double *a, const int *lda,
                            const complex_double *x, const int *incx, const complex_double *beta,
                            complex_double *y, const int *incy);
         extern void LAPACK(dgemv)(const char *joba, const int *m, const int *n,
                            const double *alpha, const double *a, const int *lda,
                            const double *x, const int *incx, const double *beta,
                            double *y, const int *incy);
         extern void LAPACK(cgemv)(const char *joba, const int *m, const int *n,
                            const complex_float *alpha, const complex_float *a, const int *lda,
                            const complex_float *x, const int *incx, const complex_float *beta,
                            complex_float *y, const int *incy);
         extern void LAPACK(sgemv)(const char *joba, const int *m, const int *n,
                            const float *alpha, const float *a, const int *lda,
                            const float *x, const int *incx, const float *beta,
                            float *y, const int *incy);

         extern void LAPACK(zhemv)(const char *uplo, const int *n,
                            const complex_double *alpha, const complex_double *a, const int *lda,
                            const complex_double *x, const int *incx, const complex_double *beta,
                            complex_double *y, const int *incy);
         extern void LAPACK(chemv)(const char *uplo, const int *n,
                            const complex_float *alpha, const complex_float *a, const int *lda,
                            const complex_float *x, const int *incx, const complex_float *beta,
                            complex_float *y, const int *incy);

         // BLAS 3
         extern void LAPACK(zgemm)(const char *joba, const char *jobb,
                            const int *m, const int *n, const int *k,
                            const complex_double *alpha, const complex_double *a, const int *lda,
                            const complex_double *b, const int *ldb,
                            const complex_double *beta, complex_double *c, const int *ldc);
         extern void LAPACK(dgemm)(const char *joba, const char *jobb,
                            const int *m, const int *n, const int *k,
                            const double *alpha, const double *a, const int *lda,
                            const double *b, const int *ldb,
                            const double *beta, double *c, const int *ldc);
         extern void LAPACK(cgemm)(const char *joba, const char *jobb, const int *m, const int *n, const int *k,
                            const complex_float *alpha, const complex_float *a, const int *lda,
                            const complex_float *b, const int *ldb,
                            const complex_float *beta, complex_float *c, const int *ldc);
         extern void LAPACK(sgemm)(const char *joba, const char *jobb, const int *m, const int *n, const int *k,
                            const float *alpha, const float *a, const int *lda,
                            const float *b, const int *ldb,
                            const float *beta, float *c, const int *ldc);

         extern void LAPACK(zhemm)(const char *side, const char *uplo,
                            const int *m, const int *n,
                            const complex_double *alpha, const complex_double *a, const int *lda,
                            const complex_double *b, const int *ldb,
                            const complex_double *beta, complex_double *c, const int *ldc);
         extern void LAPACK(chemm)(const char *side, const char *uplo,
                            const int *m, const int *n,
                            const complex_float *alpha, const complex_float *a, const int *lda,
                            const complex_float *b, const int *ldb,
                            const complex_float *beta, complex_float *c, const int *ldc);

         extern void LAPACK(ztrmm)(const char *side, const char *uplo, const char *transa, const char *diag,
                                   const int *m, const int *n, const complex_double *alpha,
                                   const complex_double *a, const int *lda, complex_double *b, const int *ldb);
         extern void LAPACK(ctrmm)(const char *side, const char *uplo, const char *transa, const char *diag,
                                   const int *m, const int *n, const complex_float *alpha,
                                   const complex_float *a, const int *lda, complex_float *b, const int *ldb);
         extern void LAPACK(dtrmm)(const char *side, const char *uplo, const char *transa, const char *diag,
                                   const int *m, const int *n, const double *alpha,
                                   const double *a, const int *lda, double *b, const int *ldb);
         extern void LAPACK(strmm)(const char *side, const char *uplo, const char *transa, const char *diag,
                                   const int *m, const int *n, const float *alpha,
                                   const float *a, const int *lda, float *b, const int *ldb);

         extern void LAPACK(zherk)(const char *uplo, const char *trans, const int *n, const int *k,
                            const complex_double *alpha, const complex_double *a, const int *lda,
                            const complex_double *beta, complex_double *c, const int *ldc);
         extern void LAPACK(cherk)(const char *uplo, const char *trans, const int *n, const int *k,
                            const complex_float *alpha, const complex_float *a, const int *lda,
                            const complex_float *beta, complex_float *c, const int *ldc);
#if MKL != 1
         // LAPACK
         extern  int LAPACK(ilaenv)(const int *ispec, const char *name, const char *opts,
                                    const int *n1, const int *n2, const int *n3, const int *n4);

         extern double LAPACK(dlamch)(const char *cmach);
         extern float  LAPACK(slamch)(const char *cmach);

         extern void LAPACK(dlabad)(double *smallValue, double *largeValue);
         extern void LAPACK(slabad)(float *smallValue, float *largeValue);

         extern double LAPACK(zlange)(const char *norm, const int *m, const int *n,
                                      const complex_double *a, const int *lda, double *rwork);
         extern double LAPACK(dlange)(const char *norm, const int *m, const int *n,
                                      const double *a, const int *lda, double *rwork);
         extern float LAPACK(clange)(const char *norm, const int *m, const int *n,
                                     const complex_float *a, const int *lda, float *rwork);
         extern float LAPACK(slange)(const char *norm, const int *m, const int *n,
                                     const float *a, const int *lda, float *rwork);

         extern void LAPACK(zgesvd)(const char *jobu, const char *jobvt,
                             const int *m, const int *n,
                             complex_double *a, const int *lda, double *s, complex_double *u,
                             const int *ldu, complex_double *vt, const int *ldvt, complex_double *work,
                             const int *lwork, double *rwork, int *info);
         extern void LAPACK(cgesvd)(const char *jobu, const char *jobvt,
                             const int *m, const int *n,
                             complex_float *a, const int *lda, float *s, complex_float *u,
                             const int *ldu, complex_float *vt, const int *ldvt, complex_float *work,
                             const int *lwork, float *rwork, int *info);
         extern void LAPACK(dgesvd)(const char *jobu, const char *jobvt,
                             const int *m, const int *n,
                             double *a, const int *lda, double *s, double *u,
                             const int *ldu, double *vt, const int *ldvt, double *work,
                             const int *lwork, int *info);
         extern void LAPACK(sgesvd)(const char *jobu, const char *jobvt,
                             const int *m, const int *n,
                             float *a, const int *lda, float *s, float *u,
                             const int *ldu, float *vt, const int *ldvt, float *work,
                             const int *lwork, int *info);

         extern void LAPACK(zgesdd)(const char *jobz, const int *m, const int *n,
                             complex_double *a, const int *lda, double *s, complex_double *u,
                             const int *ldu, complex_double *vt, const int *ldvt, complex_double *work,
                             const int *lwork, double *rwork, const int *iwork, int *info);
         extern void LAPACK(cgesdd)(const char *jobz, const int *m, const int *n,
                             complex_float *a, const int *lda, float *s, complex_float *u,
                             const int *ldu, complex_float *vt, const int *ldvt, complex_float *work,
                             const int *lwork, float *rwork, const int *iwork, int *info);
         extern void LAPACK(dgesdd)(const char *jobz, const int *m, const int *n,
                             double *a, const int *lda, double *s, double *u,
                             const int *ldu, double *vt, const int *ldvt, double *work,
                             const int *lwork, const int *iwork, int *info);
         extern void LAPACK(sgesdd)(const char *jobz, const int *m, const int *n,
                             float *a, const int *lda, float *s, float *u,
                             const int *ldu, float *vt, const int *ldvt, float *work,
                             const int *lwork, const int *iwork, int *info);

         extern void LAPACK(dgels)(const char *transa, const int *m, const int *n, const int *nrhs,
                            double *a, const int *lda, double *b, const int *ldb,
                            double *work, const int *lwork, int *info);
         extern void LAPACK(zgels)(const char *transa, const int *m, const int *n, const int *nrhs,
                            complex_double *a, const int *lda, complex_double *b, const int *ldb,
                            complex_double *work, const int *lwork, int *info);
         extern void LAPACK(sgels)(const char *transa, const int *m, const int *n, const int *nrhs,
                            float *a, const int *lda, float *b, const int *ldb,
                            float *work, const int *lwork, int *info);
         extern void LAPACK(cgels)(const char *transa, const int *m, const int *n, const int *nrhs,
                            complex_float *a, const int *lda, complex_float *b, const int *ldb,
                            complex_float *work, const int *lwork, int *info);

         extern void LAPACK(zgelsy)(const int *m, const int *n, const int *nrhs,
                             complex_double *a, const int *lda, complex_double *b, const int *ldb,
                             int *jpvt, const double *rcond, int *rank,
                             complex_double *work, const int *lwork, double *rwork, int *info);
         extern void LAPACK(dgelsy)(const int *m, const int *n, const int *nrhs,
                             double *a, const int *lda, double *b, const int *ldb,
                             int *jpvt, const double *rcond, int *rank,
                             double *work, const int *lwork, int *info);
         extern void LAPACK(cgelsy)(const int *m, const int *n, const int *nrhs,
                             complex_float *a, const int *lda, complex_float *b, const int *ldb,
                             int *jpvt, const float *rcond, int *rank,
                             complex_float *work, const int *lwork, float *rwork, int *info);
         extern void LAPACK(sgelsy)(const int *m, const int *n, const int *nrhs,
                             float *a, const int *lda, float *b, const int *ldb,
                             int *jpvt, const float *rcond, int *rank,
                             float *work, const int *lwork, int *info);

         extern void LAPACK(zgelss)(const int *m, const int *n, const int *nrhs,
                             complex_double *a, const int *lda, complex_double *b, const int *ldb,
                             double *s, const double *rcond, int *rank,
                             complex_double *work, const int *lwork, double *rwork, int *info);
         extern void LAPACK(dgelss)(const int *m, const int *n, const int *nrhs,
                             double *a, const int *lda, double *b, const int *ldb,
                             double *s, const double *rcond, int *rank,
                             double *work, const int *lwork, int *info);
         extern void LAPACK(cgelss)(const int *m, const int *n, const int *nrhs,
                             complex_float *a, const int *lda, complex_float *b, const int *ldb,
                             float *s, const float *rcond, int *rank,
                             complex_float *work, const int *lwork, float *rwork, int *info);
         extern void LAPACK(sgelss)(const int *m, const int *n, const int *nrhs,
                             float *a, const int *lda, float *b, const int *ldb,
                             float *s, const float *rcond, int *rank,
                             float *work, const int *lwork, int *info);

         extern void LAPACK(zgelsd)(const int *m, const int *n, const int *nrhs,
                             complex_double *a, const int *lda, complex_double *b, const int *ldb,
                             double *s, const double *rcond, int *rank,
                             complex_double *work, const int *lwork, double *rwork, int *iwork, int *info);
         extern void LAPACK(dgelsd)(const int *m, const int *n, const int *nrhs,
                             double *a, const int *lda, double *b, const int *ldb,
                             double *s, const double *rcond, int *rank,
                             double *work, const int *lwork, int *iwork, int *info);
         extern void LAPACK(cgelsd)(const int *m, const int *n, const int *nrhs,
                             complex_float *a, const int *lda, complex_float *b, const int *ldb,
                             float *s, const float *rcond, int *rank,
                             complex_float *work, const int *lwork, float *rwork, int *iwork, int *info);
         extern void LAPACK(sgelsd)(const int *m, const int *n, const int *nrhs,
                             float *a, const int *lda, float *b, const int *ldb,
                             float *s, const float *rcond, int *rank,
                             float *work, const int *lwork, int *iwork, int *info);

         extern void LAPACK(zgesv)(const int *n, const int *nrhs, complex_double *a, const int *lda,
                             int *ipiv, complex_double *b, const int *ldb, int *info);
         extern void LAPACK(dgesv)(const int *n, const int *nrhs, double *a, const int *lda,
                             int *ipiv, double *b, const int *ldb, int *info);
         extern void LAPACK(cgesv)(const int *n, const int *nrhs, complex_float *a, const int *lda,
                             int *ipiv, complex_float *b, const int *ldb, int *info);
         extern void LAPACK(sgesv)(const int *n, const int *nrhs, float *a, const int *lda,
                             int *ipiv, float *b, const int *ldb, int *info);

         extern void LAPACK(zposv)(const char *uplo, const int *n, const int *nrhs, complex_double *a, const int *lda,
                             complex_double *b, const int *ldb, int *info);
         extern void LAPACK(dposv)(const char *uplo, const int *n, const int *nrhs, double *a, const int *lda,
                             double *b, const int *ldb, int *info);
         extern void LAPACK(cposv)(const char *uplo, const int *n, const int *nrhs, complex_float *a, const int *lda,
                             complex_float *b, const int *ldb, int *info);
         extern void LAPACK(sposv)(const char *uplo, const int *n, const int *nrhs, float *a, const int *lda,
                             float *b, const int *ldb, int *info);

         extern void LAPACK(zheevr)(const char *jobz, const char *range, const char *uplo,
                             const int *n, complex_double *a, const int *lda,
                             const double *vl, const double *vu, const int *il, const int *iu, const double *abstol,
                             const int *m, double *w, complex_double *z, const int *ldz, int *iSuppZ,
                             complex_double *work, const int *lWork, double *rWork, const int *lrWork,
                             int *iWork, const int *liWork, int *info);
         extern void LAPACK(cheevr)(const char *jobz, const char *range, const char *uplo,
                             const int *n, complex_float *a, const int *lda,
                             const float *vl, const float *vu, const int *il, const int *iu, const float *abstol,
                             const int *m, float *w, complex_float *z, const int *ldz, int *iSuppZ,
                             complex_float *work, const int *lWork, float *rWork, const int *lrWork,
                             int *iWork, const int *liWork, int *info);

         extern void LAPACK(zheevd)(const char *jobz, const char *uplo,
                             const int *n, complex_double *a, const int *lda,
                             double *w,
                             complex_double *work, const int *lWork, double *rWork, const int *lrWork,
                             int *iWork, const int *liWork, int *info);
         extern void LAPACK(cheevd)(const char *jobz, const char *uplo,
                             const int *n, complex_float *a, const int *lda,
                             float *w,
                             complex_float *work, const int *lWork, float *rWork, const int *lrWork,
                             int *iWork, const int *liWork, int *info);

         extern void LAPACK(zheev)(const char *jobz, const char *uplo,
                             const int *n, complex_double *A, const int *lda,
                             double *w, complex_double *work, const int *lwork,
                             double *rwork, const int *info);
         extern void LAPACK(cheev)(const char *jobz, const char *uplo,
                             const int *n, complex_float *A, const int *lda,
                             float *w, complex_float *work, const int *lwork,
                             float *rwork, const int *info);

         extern void LAPACK(zgeqrf)(const int *m, const int *n, complex_double *a, const int *lda,
                                    const complex_double *tau, complex_double *work, const int *lwork, int *info);
         extern void LAPACK(dgeqrf)(const int *m, const int *n, double *a, const int *lda,
                                    const double *tau, double *work, const int *lwork, int *info);
         extern void LAPACK(cgeqrf)(const int *m, const int *n, complex_float *a, const int *lda,
                                    const complex_float *tau, complex_float *work, const int *lwork, int *info);
         extern void LAPACK(sgeqrf)(const int *m, const int *n, float *a, const int *lda,
                                    const float *tau, float *work, const int *lwork, int *info);

         extern void LAPACK(zgeqp3)(const int *m, const int *n, complex_double *a, const int *lda,
                                    int *jpvt, const complex_double *tau, complex_double *work, const int *lwork,
                                    double *rwork, int *info);
         extern void LAPACK(dgeqp3)(const int *m, const int *n, double *a, const int *lda,
                                    int *jpvt, const double *tau, double *work, const int *lwork,
                                    int *info);
         extern void LAPACK(cgeqp3)(const int *m, const int *n, complex_float *a, const int *lda,
                                    int *jpvt, const complex_float *tau, complex_float *work, const int *lwork,
                                    float *rwork, int *info);
         extern void LAPACK(sgeqp3)(const int *m, const int *n, float *a, const int *lda,
                                    int *jpvt, const float *tau, float *work, const int *lwork,
                                    int *info);

         extern void LAPACK(zgelqf)(const int *m, const int *n, complex_double *a, const int *lda,
                                    complex_double *tau, complex_double *work, const int *lwork, int *info);
         extern void LAPACK(dgelqf)(const int *m, const int *n, double *a, const int *lda,
                                    double *tau, double *work, const int *lwork, int *info);
         extern void LAPACK(cgelqf)(const int *m, const int *n, complex_float *a, const int *lda,
                                    complex_float *tau, complex_float *work, const int *lwork, int *info);
         extern void LAPACK(sgelqf)(const int *m, const int *n, float *a, const int *lda,
                                    float *tau, float *work, const int *lwork, int *info);

         extern void LAPACK(zunmlq)(const char *side, const char *trans, const int *m, const int *n, const int *k,
                                    const complex_double *a, const int *lda, const complex_double *tau, complex_double *c,
                                    const int *ldc, complex_double *work, const int *lwork, int *info);
         extern void LAPACK(cunmlq)(const char *side, const char *trans, const int *m, const int *n, const int *k,
                                    const complex_float *a, const int *lda, const complex_float *tau, complex_float *c,
                                    const int *ldc, complex_float *work, const int *lwork, int *info);

         extern void LAPACK(zunmqr)(const char *side, const char *trans, const int *m, const int *n, const int *k,
                                    const complex_double *a, const int *lda, const complex_double *tau, complex_double *c,
                                    const int *ldc, complex_double *work, const int *lwork, int *info);
         extern void LAPACK(cunmqr)(const char *side, const char *trans, const int *m, const int *n, const int *k,
                                    const complex_float *a, const int *lda, const complex_float *tau, complex_float *c,
                                    const int *ldc, complex_float *work, const int *lwork, int *info);

         extern void LAPACK(zunmrz)(const char *side, const char *trans, const int *m, const int *n, const int *k, const int *l,
                                    const complex_double *a, const int *lda, const complex_double *tau, complex_double *c,
                                    const int *ldc, complex_double *work, const int *lwork, int *info);
         extern void LAPACK(cunmrz)(const char *side, const char *trans, const int *m, const int *n, const int *k, const int *l,
                                    const complex_float *a, const int *lda, const complex_float *tau, complex_float *c,
                                    const int *ldc, complex_float *work, const int *lwork, int *info);

         extern void LAPACK(ztrtrs)(const char *uplo, const char *trans, const char *diag,
                                    const int *n, const int *nrhs, const complex_double *a, const int *lda,
                                    complex_double *b, const int *ldb, int *info);
         extern void LAPACK(dtrtrs)(const char *uplo, const char *trans, const char *diag,
                                    const int *n, const int *nrhs, const double *a, const int *lda,
                                    double *b, const int *ldb, int *info);
         extern void LAPACK(ctrtrs)(const char *uplo, const char *trans, const char *diag,
                                    const int *n, const int *nrhs, const complex_float *a, const int *lda,
                                    complex_float *b, const int *ldb, int *info);
         extern void LAPACK(strtrs)(const char *uplo, const char *trans, const char *diag,
                                    const int *n, const int *nrhs, const float *a, const int *lda,
                                    float *b, const int *ldb, int *info);

         extern void LAPACK(ztrsm)(const char *side, const char *uplo, const char *transa, const char *diag,
                                   const int *m, const int *n, const complex_double *alpha,
                                   const complex_double *a, const int *lda, complex_double *b, const int *ldb);
         extern void LAPACK(dtrsm)(const char *side, const char *uplo, const char *transa, const char *diag,
                                   const int *m, const int *n, const double *alpha,
                                   const double *a, const int *lda, double *b, const int *ldb);
         extern void LAPACK(ctrsm)(const char *side, const char *uplo, const char *transa, const char *diag,
                                   const int *m, const int *n, const complex_float *alpha,
                                   const complex_float *a, const int *lda, complex_float *b, const int *ldb);
         extern void LAPACK(strsm)(const char *side, const char *uplo, const char *transa, const char *diag,
                                   const int *m, const int *n, const float *alpha,
                                   const float *a, const int *lda, float *b, const int *ldb);

         extern void LAPACK(ztzrzf)(const int *m, const int *n, complex_double *a, const int *lda,
                                    complex_double *tau, complex_double *work, const int *lwork, int *info);
         extern void LAPACK(dtzrzf)(const int *m, const int *n, double *a, const int *lda,
                                    double *tau, double *work, const int *lwork, int *info);
         extern void LAPACK(ctzrzf)(const int *m, const int *n, complex_float *a, const int *lda,
                                    complex_float *tau, complex_float *work, const int *lwork, int *info);
         extern void LAPACK(stzrzf)(const int *m, const int *n, float *a, const int *lda,
                                    float *tau, float *work, const int *lwork, int *info);

         extern void LAPACK(zgetrf)(const int *m, const int *n, complex_double *a, const int *lda,
                                    int *ipiv, int *info);
         extern void LAPACK(dgetrf)(const int *m, const int *n, double *a, const int *lda,
                                    int *ipiv, int *info);
         extern void LAPACK(cgetrf)(const int *m, const int *n, complex_float *a, const int *lda,
                                    int *ipiv, int *info);
         extern void LAPACK(sgetrf)(const int *m, const int *n, float *a, const int *lda,
                                    int *ipiv, int *info);

         extern void LAPACK(zgetri)(const int *n, complex_double *a, const int *lda,
                                    int *ipiv, complex_double *work, const int *lwork, int *info);
         extern void LAPACK(dgetri)(const int *n, double *a, const int *lda,
                                    int *ipiv, double *work, const int *lwork, int *info);
         extern void LAPACK(cgetri)(const int *n, complex_float *a, const int *lda,
                                    int *ipiv, complex_float *work, const int *lwork, int *info);
         extern void LAPACK(sgetri)(const int *n, float *a, const int *lda,
                                    int *ipiv, float *work, const int *lwork, int *info);

         extern void LAPACK(zgetrs)(const char *trans, const int *n, const int *nrhs, complex_double *a, const int *lda,
                                    int *ipiv, complex_double *b, const int *ldb, int *info);
         extern void LAPACK(dgetrs)(const char *trans, const int *n, const int *nrhs, double *a, const int *lda,
                                    int *ipiv, double *b, const int *ldb, int *info);
         extern void LAPACK(cgetrs)(const char *trans, const int *n, const int *nrhs, complex_float *a, const int *lda,
                                    int *ipiv, complex_float *b, const int *ldb, int *info);
         extern void LAPACK(sgetrs)(const char *trans, const int *n, const int *nrhs, float *a, const int *lda,
                                    int *ipiv, float *b, const int *ldb, int *info);


         extern void LAPACK(dpotri)(const char *uplo, const int *n, double *a, const int *lda, int *info);
         extern void LAPACK(zpotri)(const char *uplo, const int *n, complex_double *a, const int *lda, int *info);
         extern void LAPACK(spotri)(const char *uplo, const int *n, float *a, const int *lda, int *info);
         extern void LAPACK(cpotri)(const char *uplo, const int *n, complex_float *a, const int *lda, int *info);

         extern void LAPACK(dpotrf)(const char *uplo, const int *n, double *a, const int *lda, int *info);
         extern void LAPACK(zpotrf)(const char *uplo, const int *n, complex_double *a, const int *lda, int *info);
         extern void LAPACK(spotrf)(const char *uplo, const int *n, float *a, const int *lda, int *info);
         extern void LAPACK(cpotrf)(const char *uplo, const int *n, complex_float *a, const int *lda, int *info);

         extern void LAPACK(dlascl)(const char *type, const int *kl, const int *ku, const double *cfrom, const double *cto,
                             const int *m, const int *n, double *a, const int *lda, int *info);
         extern void LAPACK(zlascl)(const char *type, const int *kl, const int *ku, const double *cfrom, const double *cto,
                             const int *m, const int *n, complex_double *a, const int *lda, int *info);
         extern void LAPACK(slascl)(const char *type, const int *kl, const int *ku, const float *cfrom, const float *cto,
                             const int *m, const int *n, float *a, const int *lda, int *info);
         extern void LAPACK(clascl)(const char *type, const int *kl, const int *ku, const float *cfrom, const float *cto,
                             const int *m, const int *n, complex_float *a, const int *lda, int *info);

         extern void LAPACK(zlaic1)(const int *job, const int *j, const complex_double *x, const double *sest,
                             const complex_double *w, const complex_double *gamma, double *sestpr,
                             complex_double *s, complex_double *c);
         extern void LAPACK(claic1)(const int *job, const int *j, const complex_float *x, const float *sest,
                             const complex_float *w, const complex_float *gamma, float *sestpr,
                             complex_float *s, complex_float *c);
         extern void LAPACK(dlaic1)(const int *job, const int *j, const double *x, const double *sest,
                             const double *w, const double *gamma, double *sestpr,
                             double *s, double *c);
         extern void LAPACK(slaic1)(const int *job, const int *j, const float *x, const float *sest,
                             const float *w, const float *gamma, float *sestpr,
                             float *s, float *c);

         extern void LAPACK(chetri)(const char *uplo, const int *n, complex_float *a, const int *lda,
                             const int *ipiv, complex_float *work, int *info);
         extern void LAPACK(zhetri)(const char *uplo, const int *n, complex_double *a, const int *lda,
                             const int *ipiv, complex_double *work, int *info);

         extern void LAPACK(chetrf)(const char *uplo, const int *n, complex_float *a, const int *lda,
                             int *ipiv, complex_float *work, const int *lwork, int *info);
         extern void LAPACK(zhetrf)(const char *uplo, const int *n, complex_double *a, const int *lda,
                             int *ipiv, complex_double *work, const int *lwork, int *info);

         extern void LAPACK(dsyrk)(const char *uplo, const char *trans, const int *n, const int *k,
                                   const double *alpha, const double *a, const int *lda,
                                   const double *beta, double *c, const int *ldc);
         extern void LAPACK(ssyrk)(const char *uplo, const char *trans, const int *n, const int *k,
                                   const float *alpha, const float *a, const int *lda,
                                   const float *beta, float *c, const int *ldc);
         extern void LAPACK(csyrk)(const char *uplo, const char *trans, const int *n, const int *k,
                                   const complex_float *alpha, const complex_float *a, const int *lda,
                                   const complex_float *beta, complex_float *c, const int *ldc);
         extern void LAPACK(zsyrk)(const char *uplo, const char *trans, const int *n, const int *k,
                                   const complex_double *alpha, const complex_double *a, const int *lda,
                                   const complex_double *beta, complex_double *c, const int *ldc);
#endif
      }
   } // end cpplapack_private

#if MKL
   inline void transpose(char const &trans, size_t const &m,  size_t const &n, complex_double const *A, size_t const &lda, complex_double *B, size_t const &ldb)
   {
      mkl_zomatcopy('C', trans, m, n, 1.0, A, lda, B, ldb);
   }
   inline void transpose(char const &trans, size_t const &m,  size_t const &n, double const *A, size_t const &lda, double *B, size_t const &ldb)
   {
      mkl_domatcopy('C', trans, m, n, 1.0, A, lda, B, ldb);
   }
   inline void transpose(char const &trans, size_t const &m,  size_t const &n, complex_float const *A, size_t const &lda, complex_float *B, size_t const &ldb)
   {
      mkl_comatcopy('C', trans, m, n, 1.0, A, lda, B, ldb);
   }
   inline void transpose(char const &trans, size_t const &m,  size_t const &n, float const *A, size_t const &lda, float *B, size_t const &ldb)
   {
      mkl_somatcopy('C', trans, m, n, 1.0, A, lda, B, ldb);
   }
#else
   // Templated functions result in multiple symbols
   template<typename T>
   struct conjugate
   {
      static T apply(T const &value)
      {
         return std::conj(value);
      }
   };
   template<>
   struct conjugate<double>
   {
      static double apply(double const &value)
      {
         return value;
      }
   };
   template<>
   struct conjugate<float>
   {
      static float apply(float const &value)
      {
         return value;
      }
   };


   template<typename T> void transpose(char const &trans, size_t const &m,  size_t const &n, T const *A, size_t const &lda, T *B, size_t const &ldb)
   {
      switch (trans)
      {
         case 'N':
         case 'n':
            {
               if (lda == ldb && lda == m)
               {
                  memcpy(B, A, sizeof(*B)*m*n);
               }
               else
               {
                  T const *stop_a = A + lda * n;
                  while (A < stop_a) {
                     // Copy over column
                     memcpy(B, A, sizeof(*B)*m);

                     // Goto next column
                     B += ldb;
                     A += lda;
                  }
               }
            }
            break;
         case 'T':
         case 't':
            {
               T *ptrDest;
               T *ptrDest2 = B;
               T *ptrDest3 = B + ldb * n;

               T const *ptrSrc;
               T const *ptrSrc2 = A;
               T const *ptrSrc3 = A + lda * m;

               while (ptrDest2 < ptrDest3) {

                  // Set start of column/row
                  ptrDest  = ptrDest2;
                  ptrSrc   = ptrSrc2;

                  // Loop over row, setting column values
                  while (ptrSrc < ptrSrc3) {
                     *ptrDest = *ptrSrc;
                     ptrDest++;
                     ptrSrc += lda;
                  }

                  // Goto next column/row
                  ptrDest2 += ldb;
                  ptrSrc2++;
               }
            }
            break;
         case 'C':
         case 'c':
            {

               T *ptrDest;
               T *ptrDest2 = B;
               T *ptrDest3 = B + ldb * n;

               T const *ptrSrc;
               T const *ptrSrc2 = A;
               T const *ptrSrc3 = A + lda * m;

               while (ptrDest2 != ptrDest3) {

                  // Set start of column/row
                  ptrDest  = ptrDest2;
                  ptrSrc   = ptrSrc2;

                  // Loop over row, setting column values
                  while (ptrSrc < ptrSrc3) {
                     *ptrDest = conjugate<T>().apply(*ptrSrc);
                     ptrDest++;
                     ptrSrc += lda;
                  }

                  // Goto next column/row
                  ptrDest2 += ldb;
                  ptrSrc2++;
               }
            }
            break;
         case 'R':
         case 'r':
            {
               T const *stop_a = A + lda * n;
               if (lda == ldb && lda == m)
               {
                  while (A < stop_a)
                  {
                     *B++ = conjugate<T>().apply(*A++);
                  }
               }
               else
               {
                  size_t shift_b(m - ldb);
                  size_t shift_a(m - lda);
                  T *stop_b;
                  while (A < stop_a) {
                     // Loop over column
                     stop_b = B + m;
                     while (B < stop_b) {
                        *B++ = conjugate<T>().apply(*A++);
                     }

                     // Goto next column
                     B += shift_b;
                     A += shift_a;
                  }
               }
            }

            break;
         default:
            break;
      }
   }
#endif

   template<typename T> void transpose(char const &trans, size_t const &m, size_t const &n, std::vector<T> const &A, size_t const &lda, std::vector<T> &B, size_t const &ldb)
   {
      transpose(trans, m, n, &(A.front()), lda, &(B.front()), ldb);
   }

#define CAST_VECTOR(x) &((x).front())
#define CAST_POINTER(x) x
#define TYPE_VECTOR(x) std::vector<x> &
#define TYPE_POINTER(x) x *

#define BUILD_COMPLEX_VECTOR(root) \
BUILD_FUNCTION(complex_double, VECTOR, double, z ## root, root) \
BUILD_FUNCTION(complex_float, VECTOR, float, c ## root, root)

#define BUILD_COMPLEX(root) \
BUILD_COMPLEX_VECTOR(root) \
BUILD_FUNCTION(complex_double, POINTER, double, z ## root, root) \
BUILD_FUNCTION(complex_float, POINTER, float, c ## root, root)

#define BUILD_REAL_VECTOR(root) \
BUILD_FUNCTION(float, VECTOR, float, s ## root, root) \
BUILD_FUNCTION(double, VECTOR, double, d ## root, root)

#define BUILD_REAL(root) \
BUILD_REAL_VECTOR(root) \
BUILD_FUNCTION(float, POINTER, float, s ## root, root) \
BUILD_FUNCTION(double, POINTER, double, d ## root, root)

#define BUILD_ALL_VECTOR(root) \
BUILD_COMPLEX_VECTOR(root) \
BUILD_REAL_VECTOR(root)

#define BUILD_ALL(root) \
BUILD_COMPLEX(root) \
BUILD_REAL(root)


   // BLAS 1
   /*
    *  AXPY constant times a vector plus a vector
    */
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base(int const &n, \
                    data_type const &alpha, \
                    const TYPE_ ## mem_type(data_type) x, \
                    int const &incx, \
                    TYPE_ ## mem_type(data_type) y, \
                    int const &incy \
   ) { \
      cpplapack_private::LAPACK(name)(&n, \
                                      &alpha, \
                                      CAST_ ## mem_type(x), \
                                      &incx, \
                                      CAST_ ## mem_type(y), \
                                      &incy); \
   } \
   inline void base(size_t const &s_n, \
                    data_type const &alpha, \
                    const TYPE_ ## mem_type(data_type) x, \
                    size_t const &s_incx, \
                    TYPE_ ## mem_type(data_type) y, \
                    size_t const &s_incy \
   ) { \
      int const n = static_cast<int const> (s_n); \
      int const incx = static_cast<int const> (s_incx); \
      int const incy = static_cast<int const> (s_incy); \
      cpplapack_private::LAPACK(name)(&n, \
                                      &alpha, \
                                      CAST_ ## mem_type(x), \
                                      &incx, \
                                      CAST_ ## mem_type(y), \
                                      &incy); \
   }

BUILD_ALL(axpy)


   /*
    * DOT forms the dot product of two vectors.
    * uses unrolled loops for increments equal to one.
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline data_type base( \
                         int const &n \
                       , const TYPE_ ## mem_type(data_type) x \
                       , int const &incx \
                       , TYPE_ ## mem_type(data_type) y \
                       , int const &incy \
   ) { \
      return cpplapack_private::LAPACK(name)(&n, \
                                     CAST_ ## mem_type(x), \
                                     &incx, \
                                     CAST_ ## mem_type(y), \
                                     &incy); \
   } \
   inline data_type base(size_t const &s_n, \
                   const TYPE_ ## mem_type(data_type) x, \
                   size_t const &s_incx, \
                   TYPE_ ## mem_type(data_type) y, \
                   size_t const &s_incy) \
   { \
      int const n = static_cast<int const> (s_n); \
      int const incx = static_cast<int const> (s_incx); \
      int const incy = static_cast<int const> (s_incy); \
      return cpplapack_private::LAPACK(name)(&n, \
                                      CAST_ ## mem_type(x), \
                                      &incx, \
                                      CAST_ ## mem_type(y), \
                                      &incy); \
   }

BUILD_REAL(dot)

   /*
    * DOTC forms the dot product of two complex vectors
      DOTC = X^H * Y
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline data_type base( \
         int const &n, \
         const TYPE_ ## mem_type(data_type) x, \
         int const &incx, \
         TYPE_ ## mem_type(data_type) y, \
         int const &incy) \
   { \
      return cpplapack_private::LAPACK(name)(&n, \
                                     CAST_ ## mem_type(x), \
                                     &incx, \
                                     CAST_ ## mem_type(y), \
                                     &incy); \
   } \
   inline data_type base(size_t const &s_n, \
                    const TYPE_ ## mem_type(data_type) x, \
                    size_t const &s_incx, \
                    TYPE_ ## mem_type(data_type) y, \
                    size_t const &s_incy) \
   { \
      int const n = static_cast<int const> (s_n); \
      int const incx = static_cast<int const> (s_incx); \
      int const incy = static_cast<int const> (s_incy); \
      return cpplapack_private::LAPACK(name)(&n, \
                                      CAST_ ## mem_type(x), \
                                      &incx, \
                                      CAST_ ## mem_type(y), \
                                      &incy); \
   }

BUILD_COMPLEX(dotc)

   /*
    * DOTU forms the dot product of two complex vectors
    * DOTU = X^T * Y
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline data_type base(\
         int const &n, \
         const TYPE_ ## mem_type(data_type) x, \
         int const &incx, \
         TYPE_ ## mem_type(data_type) y, \
         int const &incy) \
   { \
      return cpplapack_private::LAPACK(name)(&n, \
                                     CAST_ ## mem_type(x), \
                                     &incx, \
                                     CAST_ ## mem_type(y), \
                                     &incy); \
   } \
   inline data_type base(size_t const &s_n, \
                    const TYPE_ ## mem_type(data_type) x, \
                    size_t const &s_incx, \
                    TYPE_ ## mem_type(data_type) y, \
                    size_t const &s_incy) \
   { \
      int const n = static_cast<int const> (s_n); \
      int const incx = static_cast<int const> (s_incx); \
      int const incy = static_cast<int const> (s_incy); \
      return cpplapack_private::LAPACK(name)(&n, \
                                      CAST_ ## mem_type(x), \
                                      &incx, \
                                      CAST_ ## mem_type(y), \
                                      &incy); \
   }

BUILD_COMPLEX(dotu)

   /*
    *  NRM2 returns the euclidean norm of a vector via the function
    *  name, so that
    *
    *     NRM2 := sqrt( x'*x )
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline base_type base( \
         int const &n, \
         const TYPE_ ## mem_type(data_type) x, \
         int const &incx) \
   { \
      return cpplapack_private::LAPACK(name)(&n, \
                                             CAST_ ## mem_type(x), \
                                             &incx); \
   } \
   inline base_type base(size_t const &s_n, \
                        const TYPE_ ## mem_type(data_type) x, \
                        size_t const &s_incx) \
   { \
      int const n = static_cast<int const> (s_n); \
      int const incx = static_cast<int const> (s_incx); \
      return cpplapack_private::LAPACK(name)(&n, \
                                             CAST_ ## mem_type(x), \
                                             &incx); \
   }


BUILD_FUNCTION(complex_double, VECTOR, double, dznrm2, nrm2)
BUILD_FUNCTION(complex_double, POINTER, double, dznrm2, nrm2)
BUILD_FUNCTION(complex_float, VECTOR, float, scnrm2, nrm2)
BUILD_FUNCTION(complex_float, POINTER, float, scnrm2, nrm2)
BUILD_FUNCTION(double, VECTOR, double, dnrm2, nrm2)
BUILD_FUNCTION(double, POINTER, double, dnrm2, nrm2)
BUILD_FUNCTION(float, VECTOR, float, snrm2, nrm2)
BUILD_FUNCTION(float, POINTER, float, snrm2, nrm2)


   /*
    * SCAL scales a vector by a constant.
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base(int const &n, \
                    data_type const &alpha, \
                    TYPE_ ## mem_type(data_type) x, \
                    int const &incx) \
   { \
      cpplapack_private::LAPACK(name)(&n, \
                                      &alpha, \
                                      CAST_ ## mem_type(x), \
                                      &incx); \
   } \
   inline void base(size_t const &s_n, \
                    data_type const &alpha, \
                    TYPE_ ## mem_type(data_type) x, \
                    size_t const &s_incx) \
   { \
      int const n = static_cast<int const> (s_n); \
      int const incx = static_cast<int const> (s_incx); \
      cpplapack_private::LAPACK(name)(&n, \
                                      &alpha, \
                                      CAST_ ## mem_type(x), \
                                      &incx); \
   }

BUILD_ALL(scal)

   // BLAS 2
   /*
    *  GEMV  performs one of the matrix-vector operations
    *
    *     y := alpha*A*x + beta*y,   or   y := alpha*A**T*x + beta*y,   or
    *
    *     y := alpha*A**H*x + beta*y,
    *
    *  where alpha and beta are scalars, x and y are vectors and A is an
    *  m by n matrix.
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base(char const &trans, \
                    int const &m, \
                    int const &n, \
                    data_type const &alpha, \
                    const TYPE_ ## mem_type(data_type) a, \
                    int const &lda, \
                    const TYPE_ ## mem_type(data_type) x, \
                    int const &incx, \
                    data_type const &beta, \
                    TYPE_ ## mem_type(data_type) y, \
                    int const &incy) \
   { \
      cpplapack_private::LAPACK(name)(&trans, &m, &n, &alpha, \
                                      CAST_ ## mem_type(a), \
                                      &lda, \
                                      CAST_ ## mem_type(x), \
                                      &incx, &beta, \
                                      CAST_ ## mem_type(y), \
                                      &incy); \
   } \
   inline void base(char const &trans, \
                    size_t const &s_m, \
                    size_t const &s_n, \
                    data_type const &alpha, \
                    const TYPE_ ## mem_type(data_type) a, \
                    size_t const &s_lda, \
                    const TYPE_ ## mem_type(data_type) x, \
                    size_t const &s_incx, \
                    data_type const &beta, \
                    TYPE_ ## mem_type(data_type) y, \
                    size_t const &s_incy) \
   { \
      int const m = static_cast<int const> (s_m); \
      int const n = static_cast<int const> (s_n); \
      int const lda = static_cast<int const> (s_lda); \
      int const incx = static_cast<int const> (s_incx); \
      int const incy = static_cast<int const> (s_incy); \
      cpplapack_private::LAPACK(name)(&trans, &m, &n, &alpha, \
                                      CAST_ ## mem_type(a), \
                                      &lda, \
                                      CAST_ ## mem_type(x), \
                                      &incx, &beta, \
                                      CAST_ ## mem_type(y), \
                                      &incy); \
   }

BUILD_ALL(gemv)

   /*
    *  HEMV  performs the matrix-vector  operation
    *
    *     y := alpha*A*x + beta*y,
    *
    *  where alpha and beta are scalars, x and y are n element vectors and
    *  A is an n by n hermitian matrix.
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                    char const &uplo, \
                    int const &n, \
                    data_type const &alpha, \
                    const TYPE_ ## mem_type(data_type) a, \
                    int const &lda, \
                    const TYPE_ ## mem_type(data_type) x, \
                    int const &incx, \
                    data_type const &beta, \
                    TYPE_ ## mem_type(data_type) y, \
                    int const &incy \
   ){ \
      cpplapack_private::LAPACK(name)(&uplo,&n,&alpha, \
                                       CAST_ ## mem_type(a), \
                                       &lda, \
                                       CAST_ ## mem_type(x), \
                                       &incx,&beta, \
                                       CAST_ ## mem_type(y), \
                                       &incy);\
   } \
   inline void base( \
                    char const &uplo, \
                    size_t const &s_n, \
                    data_type const &alpha, \
                    const TYPE_ ## mem_type(data_type) a, \
                    size_t const &s_lda, \
                    const TYPE_ ## mem_type(data_type) x, \
                    size_t const &s_incx, \
                    data_type const &beta, \
                    TYPE_ ## mem_type(data_type) y, \
                    size_t const &s_incy \
   ){ \
      int const n = static_cast<int const> (s_n); \
      int const lda = static_cast<int const> (s_lda); \
      int const incx = static_cast<int const> (s_incx); \
      int const incy = static_cast<int const> (s_incy); \
      cpplapack_private::LAPACK(name)(&uplo,&n,&alpha, \
                                       CAST_ ## mem_type(a), \
                                       &lda, \
                                       CAST_ ## mem_type(x), \
                                       &incx,&beta, \
                                       CAST_ ## mem_type(y), \
                                       &incy);\
   }

BUILD_COMPLEX(hemv)



   // BLAS 3
   /*
    *  GEMM  performs one of the matrix-matrix operations
    *
    *     C := alpha*op( A )*op( B ) + beta*C,
    *
    *  where  op( X ) is one of
    *
    *     op( X ) = X   or   op( X ) = X**T   or   op( X ) = X**H,
    *
    *  alpha and beta are scalars, and A, B and C are matrices, with op( A )
    *  an m by k matrix,  op( B )  a  k by n matrix and  C an m by n matrix.
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type,base_type, name, base) \
   inline void base( \
                    char const &transa, \
                    char const &transb, \
                    int const &m, \
                    int const &n, \
                    int const &k, \
                    data_type const &alpha, \
                    const TYPE_ ## mem_type(data_type) a, \
                    int const &lda, \
                    const TYPE_ ## mem_type(data_type) b, \
                    int const &ldb, \
                    data_type const &beta, \
                    TYPE_ ## mem_type(data_type) c, \
                    int const &ldc \
   ){ \
      cpplapack_private::LAPACK(name)( \
                                      &transa, &transb, &m, &n, &k, &alpha, \
                                      CAST_ ## mem_type(a), \
                                      &lda, \
                                      CAST_ ## mem_type(b), \
                                      &ldb, \
                                      &beta, \
                                      CAST_ ## mem_type(c), \
                                      &ldc \
                                      ); \
   } \
   inline void base( \
                    char const &transa, \
                    char const &transb, \
                    size_t const &s_m, \
                    size_t const &s_n, \
                    size_t const &s_k, \
                    data_type const &alpha, \
                    const TYPE_ ## mem_type(data_type) a, \
                    size_t const &s_lda, \
                    const TYPE_ ## mem_type(data_type) b, \
                    size_t const &s_ldb, \
                    data_type const &beta, \
                    TYPE_ ## mem_type(data_type) c, \
                    size_t const &s_ldc \
   ){ \
      int const m = static_cast<int const> (s_m); \
      int const n = static_cast<int const> (s_n); \
      int const k = static_cast<int const> (s_k); \
      int const lda = static_cast<int const> (s_lda); \
      int const ldb = static_cast<int const> (s_ldb); \
      int const ldc = static_cast<int const> (s_ldc); \
      cpplapack_private::LAPACK(name)( \
                                      &transa, &transb, &m, &n, &k, &alpha, \
                                      CAST_ ## mem_type(a), \
                                      &lda, \
                                      CAST_ ## mem_type(b), \
                                      &ldb, \
                                      &beta, \
                                      CAST_ ## mem_type(c), \
                                      &ldc \
                                      ); \
   }

BUILD_ALL(gemm)

   /*
    * ZHEMM  performs one of the matrix-matrix operations
    *
    *     C := alpha*A*B + beta*C,
    *
    *  or
    *
    *     C := alpha*B*A + beta*C,
    *
    * where alpha and beta are scalars, A is an hermitian matrix
    * and B and C are m by n matrices.
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                    char const &side, \
                    char const &uplo, \
                    int const &m, \
                    int const &n, \
                    data_type const &alpha, \
                    const TYPE_ ## mem_type(data_type) a, \
                    int const &lda, \
                    const TYPE_ ## mem_type(data_type) b, \
                    int const &ldb, \
                    data_type const &beta, \
                    TYPE_ ## mem_type(data_type) c, \
                    int const &ldc \
   ){ \
      cpplapack_private::LAPACK(name)( \
                                      &side, &uplo, &m, &n, &alpha, \
                                      CAST_ ## mem_type(a), \
                                      &lda, \
                                      CAST_ ## mem_type(b), \
                                      &ldb, \
                                      &beta, \
                                      CAST_ ## mem_type(c), \
                                      &ldc \
                                      ); \
   } \
   inline void base( \
                    char const &side, \
                    char const &uplo, \
                    size_t const &s_m, \
                    size_t const &s_n, \
                    data_type const &alpha, \
                    const TYPE_ ## mem_type(data_type) a, \
                    size_t const &s_lda, \
                    const TYPE_ ## mem_type(data_type) b, \
                    size_t const &s_ldb, \
                    data_type const &beta, \
                    TYPE_ ## mem_type(data_type) c, \
                    size_t const &s_ldc \
   ){ \
      int const m = static_cast<int const> (s_m); \
      int const n = static_cast<int const> (s_n); \
      int const lda = static_cast<int const> (s_lda); \
      int const ldb = static_cast<int const> (s_ldb); \
      int const ldc = static_cast<int const> (s_ldc); \
      cpplapack_private::LAPACK(name)( \
                                      &side, &uplo, &m, &n, &alpha, \
                                      CAST_ ## mem_type(a), \
                                      &lda, \
                                      CAST_ ## mem_type(b), \
                                      &ldb, \
                                      &beta, \
                                      CAST_ ## mem_type(c), \
                                      &ldc \
                                      ); \
   }

BUILD_COMPLEX(hemm)


   /*
    * TRMM  performs one of the matrix-matrix operations
    *
    *    B := alpha*op( A )*B,   or   B := alpha*B*op( A )
    *
    * where  alpha  is a scalar,  B  is an m by n matrix,  A  is a unit, or
    * non-unit,  upper or lower triangular matrix  and  op( A )  is one  of
    *
    *    op( A ) = A   or   op( A ) = A**T   or   op( A ) = A**H.
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                    char const &side, \
                    char const &uplo, \
                    char const &transa, \
                    char const &diag, \
                    int const &m, \
                    int const &n, \
                    data_type const &alpha, \
                    const TYPE_ ## mem_type(data_type) a, \
                    int const &lda, \
                    TYPE_ ## mem_type(data_type) b, \
                    int const &ldb \
   ){ \
      cpplapack_private::LAPACK(name)( \
                                      &side, &uplo, &transa, &diag, &m, &n, &alpha, \
                                      CAST_ ## mem_type(a), \
                                      &lda, \
                                      CAST_ ## mem_type(b), \
                                      &ldb \
                                      ); \
   } \
   inline void base( \
                    char const &side, \
                    char const &uplo, \
                    char const &transa, \
                    char const &diag, \
                    size_t const &s_m, \
                    size_t const &s_n, \
                    data_type const &alpha, \
                    const TYPE_ ## mem_type(data_type) a, \
                    size_t const &s_lda, \
                    TYPE_ ## mem_type(data_type) b, \
                    size_t const &s_ldb \
   ){ \
      int const m = static_cast<int const> (s_m); \
      int const n = static_cast<int const> (s_n); \
      int const lda = static_cast<int const> (s_lda); \
      int const ldb = static_cast<int const> (s_ldb); \
      cpplapack_private::LAPACK(name)( \
                                      &side, &uplo, &transa, &diag, &m, &n, &alpha, \
                                      CAST_ ## mem_type(a), \
                                      &lda, \
                                      CAST_ ## mem_type(b), \
                                      &ldb \
                                      ); \
   }

BUILD_ALL(trmm)


   /*
    * SYRK performs one of the symmetric rank k operations
    *
    *    C := alpha*A*A**T + beta*C,
    *
    * or
    *
    *    C := alpha*A**T*A + beta*C,
    *
    * where alpha and beta are scalars, C is an n by n symmetric matrix
    * and A is an n by k matrix in the first case and a k by n matrix
    * in the second case.
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                    char const &uplo, \
                    char const &trans, \
                    int const &n, \
                    int const &k, \
                    data_type const &alpha, \
                    const TYPE_ ## mem_type(data_type) a, \
                    int const &lda, \
                    data_type const &beta, \
                    TYPE_ ## mem_type(data_type) c, \
                    int const &ldc \
   ){ \
      cpplapack_private::LAPACK(name)( \
                                      &uplo, &trans, &n, &k, &alpha, \
                                      CAST_ ## mem_type(a), \
                                      &lda, \
                                      &beta, \
                                      CAST_ ## mem_type(c), \
                                      &ldc \
                                      ); \
   } \
   inline void base( \
                    char const &uplo, \
                    char const &trans, \
                    size_t const &s_n, \
                    size_t const &s_k, \
                    data_type const &alpha, \
                    const TYPE_ ## mem_type(data_type) a, \
                    size_t const &s_lda, \
                    data_type const &beta, \
                    TYPE_ ## mem_type(data_type) c, \
                    size_t const &s_ldc \
   ){ \
      int const n = static_cast<int const> (s_n); \
      int const k = static_cast<int const> (s_k); \
      int const lda = static_cast<int const> (s_lda); \
      int const ldc = static_cast<int const> (s_ldc); \
      cpplapack_private::LAPACK(name)( \
                                      &uplo, &trans, &n, &k, &alpha, \
                                      CAST_ ## mem_type(a), \
                                      &lda, \
                                      &beta, \
                                      CAST_ ## mem_type(c), \
                                      &ldc \
                                      ); \
   }

BUILD_ALL(syrk)

   /*
    *  HERK  performs one of the hermitian rank k operations
    *
    *     C := alpha*A*A**H + beta*C,
    *
    *  or
    *
    *     C := alpha*A**H*A + beta*C,
    *
    *  where  alpha and beta  are  real scalars,  C is an  n by n  hermitian
    *  matrix and  A  is an  n by k  matrix in the  first case and a  k by n
    *  matrix in the second case.
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                    char const &uplo, \
                    char const &trans, \
                    int const &n, \
                    int const &k, \
                    data_type const &alpha, \
                    const TYPE_ ## mem_type(data_type) a, \
                    int const &lda, \
                    data_type const &beta, \
                    TYPE_ ## mem_type(data_type) c, \
                    int const &ldc \
   ){ \
      cpplapack_private::LAPACK(name)( \
                                      &uplo, &trans, &n, &k, &alpha, \
                                      CAST_ ## mem_type(a), \
                                      &lda, \
                                      &beta, \
                                      CAST_ ## mem_type(c), \
                                      &ldc \
                                      ); \
   } \
   inline void base( \
                    char const &uplo, \
                    char const &trans, \
                    size_t const &s_n, \
                    size_t const &s_k, \
                    data_type const &alpha, \
                    const TYPE_ ## mem_type(data_type) a, \
                    size_t const &s_lda, \
                    data_type const &beta, \
                    TYPE_ ## mem_type(data_type) c, \
                    size_t const &s_ldc \
   ){ \
      int const n = static_cast<int const> (s_n); \
      int const k = static_cast<int const> (s_k); \
      int const lda = static_cast<int const> (s_lda); \
      int const ldc = static_cast<int const> (s_ldc); \
      cpplapack_private::LAPACK(name)( \
                                      &uplo, &trans, &n, &k, &alpha, \
                                      CAST_ ## mem_type(a), \
                                      &lda, \
                                      &beta, \
                                      CAST_ ## mem_type(c), \
                                      &ldc \
                                      ); \
   }

BUILD_COMPLEX(herk)


   // LAPACK
   /*
    *  ILAENV is called from the LAPACK routines to choose problem-dependent
    *  parameters for the local environment.  See ISPEC for a description of
    *  the parameters.
    *
    *  ILAENV returns an INTEGER
    *  if ILAENV >= 0: ILAENV returns the value of the parameter specified by ISPEC
    *  if ILAENV < 0:  if ILAENV = -k, the k-th argument had an illegal value.
    *
    *  This version provides a set of parameters which should give good,
    *  but not optimal, performance on many of the currently available
    *  computers.  Users are encouraged to modify this subroutine to set
    *  the tuning parameters for their particular machine using the option
    *  and problem size information in the arguments.
    *
    *  This routine will not function correctly if it is converted to all
    *  lower case.  Converting it to all upper case is allowed.
    */
   inline int ilaenv(int const &ispec, char const *name, char const *opts,
                     int const &n1, int const &n2, int const &n3, int const &n4)
   {
      int retval;
#if MKL
      retval = SOURCE(ilaenv)(ispec, name, opts, n1, n2, n3, n4);
#else
      retval = SOURCE(ilaenv)(&ispec, name, opts, &n1, &n2, &n3, &n4);
#endif
      if (retval < 0)
         LAPACK_ERROR(__FUNCTION__, "Invalid argument", retval);
      return retval;
   }

   inline size_t ilaenv(size_t const &s_ispec, char const *name, char const *opts,
                        size_t const &s_n1, size_t const &s_n2, size_t const &s_n3, size_t const &s_n4)
   {
      int const ispec = static_cast<int const>(s_ispec);
      int const n1 = static_cast<int const>(s_n1);
      int const n2 = static_cast<int const>(s_n2);
      int const n3 = static_cast<int const>(s_n3);
      int const n4 = static_cast<int const>(s_n4);
      return static_cast<size_t>(ilaenv(ispec, name, opts, n1, n2, n3, n4));
   }

   /*
    *  LAMCH determines precision machine parameters.
    */
   inline void lamch(char const &cmach, double &val)
   {
       val = SOURCE(dlamch)(&cmach);
   }
   inline void lamch(char const &cmach, float &val)
   {
       val = SOURCE(slamch)(&cmach);
   }


   /*
    *  LABAD takes as input the values computed by LAMCH for underflow and
    *  overflow, and returns the square root of each of these values if the
    *  log of LARGE is sufficiently large.  This subroutine is intended to
    *  identify machines with a large exponent range, such as the Crays, and
    *  redefine the underflow and overflow limits to be the square roots of
    *  the values computed by LAMCH.  This subroutine is needed because
    *  LAMCH does not compensate for poor arithmetic in the upper half of
    *  the exponent range, as is found on a Cray.
    */
   inline void labad(double &smallValue, double &largeValue)
   {
       SOURCE(dlabad)(&smallValue, &largeValue);
   }
   inline void labad(float &smallValue, float &largeValue)
   {
       SOURCE(slabad)(&smallValue, &largeValue);
   }

   /*
    *  LANGE  returns the value of the one norm,  or the Frobenius norm, or
    *  the  infinity norm,  or the  element of  largest absolute value  of a
    *  complex matrix A.
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline base_type lange( \
                         char const &norm, \
                         int const &m, \
                         int const &n, \
                         const TYPE_ ## mem_type(data_type) a, \
                         int const &lda, \
                         TYPE_ ## mem_type(base_type) rwork \
   ) { \
      return SOURCE(name)( \
                          &norm, &m, &n, \
                          CAST_ ## mem_type(a), \
                          &lda, \
                          CAST_ ## mem_type(rwork) \
                          ); \
   } \
   inline base_type lange( \
                         char const &norm, \
                         size_t const &s_m, \
                         size_t const &s_n, \
                         const TYPE_ ## mem_type(data_type) a, \
                         size_t const &s_lda, \
                         TYPE_ ## mem_type(base_type) rwork \
   ) { \
      int const m = static_cast<int const>(s_m); \
      int const n = static_cast<int const>(s_n); \
      int const lda = static_cast<int const>(s_lda); \
      return SOURCE(name)( \
                          &norm, &m, &n, \
                          CAST_ ## mem_type(a), \
                          &lda, \
                          CAST_ ## mem_type(rwork) \
                          ); \
   }

BUILD_FUNCTION(complex_double, VECTOR, double, zlange, lange)
BUILD_FUNCTION(complex_double, POINTER, double, zlange, lange)
BUILD_FUNCTION(complex_float, VECTOR, float, clange, lange)
BUILD_FUNCTION(complex_float, POINTER, float, clange, lange)
BUILD_FUNCTION(double, VECTOR, double, dlange, lange)
BUILD_FUNCTION(double, POINTER, double, dlange, lange)
BUILD_FUNCTION(float, VECTOR, float, slange, lange)
BUILD_FUNCTION(float, POINTER, float, slange, lange)

#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline base_type lange( \
                         char const &norm, \
                         int const &m, \
                         int const &n, \
                         const TYPE_ ## mem_type(data_type) a, \
                         int const &lda \
   ) { \
      assert((norm != 'I' && norm != 'i')); \
      return SOURCE(name)( \
                          &norm, &m, &n, \
                          CAST_ ## mem_type(a), \
                          &lda, \
                          nullptr \
                          ); \
   }

BUILD_FUNCTION(complex_double, VECTOR, double, zlange, lange)
BUILD_FUNCTION(complex_double, POINTER, double, zlange, lange)
BUILD_FUNCTION(complex_float, VECTOR, float, clange, lange)
BUILD_FUNCTION(complex_float, POINTER, float, clange, lange)
BUILD_FUNCTION(double, VECTOR, double, dlange, lange)
BUILD_FUNCTION(double, POINTER, double, dlange, lange)
BUILD_FUNCTION(float, VECTOR, float, slange, lange)
BUILD_FUNCTION(float, POINTER, float, slange, lange)


   /*
    *  GESVD computes the singular value decomposition (SVD) of a complex
    *  M-by-N matrix A, optionally computing the left and/or right singular
    *  vectors. The SVD is written
    *
    *       A = U * SIGMA * conjugate-transpose(V)
    *
    *  where SIGMA is an M-by-N matrix which is zero except for its
    *  min(m,n) diagonal elements, U is an M-by-M unitary matrix, and
    *  V is an N-by-N unitary matrix.  The diagonal elements of SIGMA
    *  are the singular values of A; they are real and non-negative, and
    *  are returned in descending order.  The first min(m,n) columns of
    *  U and V are the left and right singular vectors of A.
    *
    *  Note that the routine returns V**H, not V.
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                     char const &jobu, \
                     char const &jobvt, \
                     int const &m, \
                     int const &n, \
                     TYPE_ ## mem_type(data_type) a, \
                     int const &lda, \
                     TYPE_ ## mem_type(base_type) s, \
                     TYPE_ ## mem_type(data_type) u, \
                     int const &ldu, \
                     TYPE_ ## mem_type(data_type) vt, \
                     int const &ldvt, \
                     TYPE_ ## mem_type(data_type) work, \
                     int const &lwork, \
                     TYPE_ ## mem_type(base_type) rwork \
   ) { \
      int info(0); \
      SOURCE(name)(&jobu, &jobvt, &m, &n, \
                   CAST_ ## mem_type(a), \
                   &lda, \
                   CAST_ ## mem_type(s), \
                   CAST_ ## mem_type(u), \
                   &ldu, \
                   CAST_ ## mem_type(vt), \
                   &ldvt, \
                   CAST_ ## mem_type(work), \
                   &lwork, \
                   CAST_ ## mem_type(rwork), \
                   &info); \
      LAPACK_ERROR(__FUNCTION__, "Did not converge", info); \
   } \
   inline void base( \
                     char const &jobu, \
                     char const &jobvt, \
                     size_t const &s_m, \
                     size_t const &s_n, \
                     TYPE_ ## mem_type(data_type) a, \
                     size_t const &s_lda, \
                     TYPE_ ## mem_type(base_type) s, \
                     TYPE_ ## mem_type(data_type) u, \
                     size_t const &s_ldu, \
                     TYPE_ ## mem_type(data_type) vt, \
                     size_t const &s_ldvt, \
                     TYPE_ ## mem_type(data_type) work, \
                     int const &lwork, \
                     TYPE_ ## mem_type(base_type) rwork \
   ) { \
      int const m = static_cast<int const>(s_m); \
      int const n = static_cast<int const>(s_n); \
      int const lda = static_cast<int const>(s_lda); \
      int const ldu = static_cast<int const>(s_ldu); \
      int const ldvt = static_cast<int const>(s_ldvt); \
      int info = 0; \
      SOURCE(name)(&jobu, &jobvt, &m, &n, \
                   CAST_ ## mem_type(a), \
                   &lda, \
                   CAST_ ## mem_type(s), \
                   CAST_ ## mem_type(u), \
                   &ldu, \
                   CAST_ ## mem_type(vt), \
                   &ldvt, \
                   CAST_ ## mem_type(work), \
                   &lwork, \
                   CAST_ ## mem_type(rwork), \
                   &info); \
      LAPACK_ERROR(__FUNCTION__, "Did not converge", info); \
   }

BUILD_COMPLEX(gesvd)


#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                     char const &jobu, \
                     char const &jobvt, \
                     int const &m, \
                     int const &n, \
                     TYPE_ ## mem_type(data_type) a, \
                     int const &lda, \
                     TYPE_ ## mem_type(base_type) s, \
                     TYPE_ ## mem_type(data_type) u, \
                     int const &ldu, \
                     TYPE_ ## mem_type(data_type) vt, \
                     int const &ldvt, \
                     TYPE_ ## mem_type(data_type) work, \
                     int const &lwork \
   ) { \
      int info = 0; \
      SOURCE(name)(&jobu, &jobvt, &m, &n, \
                   CAST_ ## mem_type(a), \
                   &lda, \
                   CAST_ ## mem_type(s), \
                   CAST_ ## mem_type(u), \
                   &ldu, \
                   CAST_ ## mem_type(vt), \
                   &ldvt, \
                   CAST_ ## mem_type(work), \
                   &lwork, \
                   &info); \
      LAPACK_ERROR(__FUNCTION__, "Did not converge", info); \
   } \
   inline void base( \
                     char const &jobu, \
                     char const &jobvt, \
                     size_t const &s_m, \
                     size_t const &s_n, \
                     TYPE_ ## mem_type(data_type) a, \
                     size_t const &s_lda, \
                     TYPE_ ## mem_type(base_type) s, \
                     TYPE_ ## mem_type(data_type) u, \
                     size_t const &s_ldu, \
                     TYPE_ ## mem_type(data_type) vt, \
                     size_t const &s_ldvt, \
                     TYPE_ ## mem_type(data_type) work, \
                     int const &lwork \
   ) { \
      int const m = static_cast<int const>(s_m); \
      int const n = static_cast<int const>(s_n); \
      int const lda = static_cast<int const>(s_lda); \
      int const ldu = static_cast<int const>(s_ldu); \
      int const ldvt = static_cast<int const>(s_ldvt); \
      int info = 0; \
      SOURCE(name)(&jobu, &jobvt, &m, &n, \
                   CAST_ ## mem_type(a), \
                   &lda, \
                   CAST_ ## mem_type(s), \
                   CAST_ ## mem_type(u), \
                   &ldu, \
                   CAST_ ## mem_type(vt), \
                   &ldvt, \
                   CAST_ ## mem_type(work), \
                   &lwork, \
                   &info); \
      LAPACK_ERROR(__FUNCTION__, "Did not converge", info); \
   }

BUILD_REAL(gesvd)


   /*
    *  GESDD computes the singular value decomposition (SVD) of a complex
    *  M-by-N matrix A, optionally computing the left and/or right singular
    *  vectors, by using divide-and-conquer method. The SVD is written
    *
    *       A = U * SIGMA * conjugate-transpose(V)
    *
    *  where SIGMA is an M-by-N matrix which is zero except for its
    *  min(m,n) diagonal elements, U is an M-by-M unitary matrix, and
    *  V is an N-by-N unitary matrix.  The diagonal elements of SIGMA
    *  are the singular values of A; they are real and non-negative, and
    *  are returned in descending order.  The first min(m,n) columns of
    *  U and V are the left and right singular vectors of A.
    *
    *  Note that the routine returns VT = V**H, not V.
    *
    *  The divide and conquer algorithm makes very mild assumptions about
    *  floating point arithmetic. It will work on machines with a guard
    *  digit in add/subtract, or on those binary machines without guard
    *  digits which subtract like the Cray X-MP, Cray Y-MP, Cray C-90, or
    *  Cray-2. It could conceivably fail on hexadecimal or decimal machines
    *  without guard digits, but we know of none.
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                     char const &jobz, \
                     int const &m, \
                     int const &n, \
                     TYPE_ ## mem_type(data_type) a, \
                     int const &lda, \
                     TYPE_ ## mem_type(base_type) s, \
                     TYPE_ ## mem_type(data_type) u, \
                     int const &ldu, \
                     TYPE_ ## mem_type(data_type) vt, \
                     int const &ldvt, \
                     TYPE_ ## mem_type(data_type) work, \
                     int const &lwork, \
                     TYPE_ ## mem_type(base_type) rwork, \
                     TYPE_ ## mem_type(int) iwork \
   ) { \
      int info = 0; \
      SOURCE(name)(&jobz, &m, &n, \
                   CAST_ ## mem_type(a), \
                   &lda, \
                   CAST_ ## mem_type(s), \
                   CAST_ ## mem_type(u), \
                   &ldu, \
                   CAST_ ## mem_type(vt), \
                   &ldvt, \
                   CAST_ ## mem_type(work), \
                   &lwork, \
                   CAST_ ## mem_type(rwork), \
                   CAST_ ## mem_type(iwork), \
                   &info); \
      LAPACK_ERROR(__FUNCTION__, "Did not converge", info); \
   } \
   inline void base( \
                     char const &jobz, \
                     size_t const &s_m, \
                     size_t const &s_n, \
                     TYPE_ ## mem_type(data_type) a, \
                     size_t const &s_lda, \
                     TYPE_ ## mem_type(base_type) s, \
                     TYPE_ ## mem_type(data_type) u, \
                     size_t const &s_ldu, \
                     TYPE_ ## mem_type(data_type) vt, \
                     size_t const &s_ldvt, \
                     TYPE_ ## mem_type(data_type) work, \
                     int const &lwork, \
                     TYPE_ ## mem_type(base_type) rwork, \
                     TYPE_ ## mem_type(int) iwork \
   ) { \
      int const m = static_cast<int const>(s_m); \
      int const n = static_cast<int const>(s_n); \
      int const lda = static_cast<int const>(s_lda); \
      int const ldu = static_cast<int const>(s_ldu); \
      int const ldvt = static_cast<int const>(s_ldvt); \
      int info = 0; \
      SOURCE(name)(&jobz, &m, &n, \
                   CAST_ ## mem_type(a), \
                   &lda, \
                   CAST_ ## mem_type(s), \
                   CAST_ ## mem_type(u), \
                   &ldu, \
                   CAST_ ## mem_type(vt), \
                   &ldvt, \
                   CAST_ ## mem_type(work), \
                   &lwork, \
                   CAST_ ## mem_type(rwork), \
                   CAST_ ## mem_type(iwork), \
                   &info); \
      LAPACK_ERROR(__FUNCTION__, "Did not converge", info); \
   }

BUILD_COMPLEX(gesdd)


#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                     char const &jobz, \
                     int const &m, \
                     int const &n, \
                     TYPE_ ## mem_type(data_type) a, \
                     int const &lda, \
                     TYPE_ ## mem_type(base_type) s, \
                     TYPE_ ## mem_type(data_type) u, \
                     int const &ldu, \
                     TYPE_ ## mem_type(data_type) vt, \
                     int const &ldvt, \
                     TYPE_ ## mem_type(data_type) work, \
                     int const &lwork, \
                     TYPE_ ## mem_type(int) iwork \
   ) { \
      int info = 0; \
      SOURCE(name)(&jobz, &m, &n, \
                   CAST_ ## mem_type(a), \
                   &lda, \
                   CAST_ ## mem_type(s), \
                   CAST_ ## mem_type(u), \
                   &ldu, \
                   CAST_ ## mem_type(vt), \
                   &ldvt, \
                   CAST_ ## mem_type(work), \
                   &lwork, \
                   CAST_ ## mem_type(iwork), \
                   &info); \
      LAPACK_ERROR(__FUNCTION__, "Did not converge", info); \
   } \
   inline void base( \
                     char const &jobz, \
                     size_t const &s_m, \
                     size_t const &s_n, \
                     TYPE_ ## mem_type(data_type) a, \
                     size_t const &s_lda, \
                     TYPE_ ## mem_type(base_type) s, \
                     TYPE_ ## mem_type(data_type) u, \
                     size_t const &s_ldu, \
                     TYPE_ ## mem_type(data_type) vt, \
                     size_t const &s_ldvt, \
                     TYPE_ ## mem_type(data_type) work, \
                     int const &lwork, \
                     TYPE_ ## mem_type(int) iwork \
   ) { \
      int const m = static_cast<int const>(s_m); \
      int const n = static_cast<int const>(s_n); \
      int const lda = static_cast<int const>(s_lda); \
      int const ldu = static_cast<int const>(s_ldu); \
      int const ldvt = static_cast<int const>(s_ldvt); \
      int info = 0; \
      SOURCE(name)(&jobz, &m, &n, \
                   CAST_ ## mem_type(a), \
                   &lda, \
                   CAST_ ## mem_type(s), \
                   CAST_ ## mem_type(u), \
                   &ldu, \
                   CAST_ ## mem_type(vt), \
                   &ldvt, \
                   CAST_ ## mem_type(work), \
                   &lwork, \
                   CAST_ ## mem_type(iwork), \
                   &info); \
      LAPACK_ERROR(__FUNCTION__, "Did not converge", info); \
   }

BUILD_REAL(gesdd)

   /*
    *  GELS solves overdetermined or underdetermined complex linear systems
    *  involving an M-by-N matrix A, or its conjugate-transpose, using a QR
    *  or LQ factorization of A.  It is assumed that A has full rank.
    *
    *  The following options are provided:
    *
    *  1. If TRANS = 'N' and m >= n:  find the least squares solution of
    *     an overdetermined system, i.e., solve the least squares problem
    *                  minimize || B - A*X ||.
    *
    *  2. If TRANS = 'N' and m < n:  find the minimum norm solution of
    *     an underdetermined system A * X = B.
    *
    *  3. If TRANS = 'C' and m >= n:  find the minimum norm solution of
    *     an undetermined system A**H * X = B.
    *
    *  4. If TRANS = 'C' and m < n:  find the least squares solution of
    *     an overdetermined system, i.e., solve the least squares problem
    *                  minimize || B - A**H * X ||.
    *
    *  Several right hand side vectors b and solution vectors x can be
    *  handled in a single call; they are stored as the columns of the
    *  M-by-NRHS right hand side matrix B and the N-by-NRHS solution
    *  matrix X.
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                    char const &transa, \
                    int const &m, \
                    int const &n, \
                    int const &nrhs, \
                    TYPE_ ## mem_type(data_type) a, \
                    int const &lda, \
                    TYPE_ ## mem_type(data_type) b, \
                    int const &ldb, \
                    TYPE_ ## mem_type(data_type) work, \
                    int const &lwork \
   ) { \
      int info = 0; \
      SOURCE(name)(&transa, &m, &n, &nrhs, \
                   CAST_ ## mem_type(a), \
                   &lda, \
                   CAST_ ## mem_type(b), \
                   &ldb, \
                   CAST_ ## mem_type(work), \
                   &lwork, \
                   &info); \
      LAPACK_ERROR(__FUNCTION__, "Not full rank", info); \
   } \
   inline void base( \
                    char const &transa, \
                    size_t const &s_m, \
                    size_t const &s_n, \
                    size_t const &s_nrhs, \
                    TYPE_ ## mem_type(data_type) a, \
                    size_t const &s_lda, \
                    TYPE_ ## mem_type(data_type) b, \
                    size_t const &s_ldb, \
                    TYPE_ ## mem_type(data_type) work, \
                    size_t const &s_lwork \
   ) { \
      int const m = static_cast<int const>(s_m); \
      int const n = static_cast<int const>(s_n); \
      int const nrhs = static_cast<int const>(s_nrhs); \
      int const lda = static_cast<int const>(s_lda); \
      int const ldb = static_cast<int const>(s_ldb); \
      int const lwork = static_cast<int const>(s_lwork); \
      int info = 0; \
      SOURCE(name)(&transa, &m, &n, &nrhs, \
                   CAST_ ## mem_type(a), \
                   &lda, \
                   CAST_ ## mem_type(b), \
                   &ldb, \
                   CAST_ ## mem_type(work), \
                   &lwork, \
                   &info); \
      LAPACK_ERROR(__FUNCTION__, "Not full rank", info); \
   }

BUILD_ALL(gels)


   /*
    *  DGELSY computes the minimum-norm solution to a real linear least
    *  squares problem:
    *      min || A * X - B ||
    *  using a complete orthogonal factorization of A.  A is an M-by-N
    *  matrix which may be rank-deficient.
    *
    *  Several right hand side vectors b and solution vectors x can be
    *  handled in a single call; they are stored as the columns of the
    *  M-by-NRHS right hand side matrix B and the N-by-NRHS solution
    *  matrix X.
    *
    *  The routine first computes a QR factorization with column pivoting:
    *      A * P = Q * [ R11 R12 ]
    *                  [  0  R22 ]
    *  with R11 defined as the largest leading submatrix whose estimated
    *  condition number is less than 1/RCOND.  The order of R11, RANK,
    *  is the effective rank of A.
    *
    *  Then, R22 is considered to be negligible, and R12 is annihilated
    *  by orthogonal transformations from the right, arriving at the
    *  complete orthogonal factorization:
    *     A * P = Q * [ T11 0 ] * Z
    *                 [  0  0 ]
    *  The minimum-norm solution is then
    *     X = P * Z' [ inv(T11)*Q1'*B ]
    *                [        0       ]
    *  where Q1 consists of the first RANK columns of Q.
    *
    *  This routine is basically identical to the original xGELSX except
    *  three differences:
    *    o The call to the subroutine xGEQPF has been substituted by the
    *      the call to the subroutine xGEQP3. This subroutine is a Blas-3
    *      version of the QR factorization with column pivoting.
    *    o Matrix B (the right hand side) is updated with Blas-3.
    *    o The permutation of matrix B (the right hand side) is faster and
    *      more simple.
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                     int const &m, \
                     int const &n, \
                     int const &nrhs, \
                     TYPE_ ## mem_type(data_type) a, \
                     int const &lda, \
                     TYPE_ ## mem_type(data_type) b, \
                     int const &ldb, \
                     TYPE_ ## mem_type(int) jpvt, \
                     base_type const &rcond, \
                     int &rank, \
                     TYPE_ ## mem_type(data_type) work, \
                     int const &lwork, \
                     TYPE_## mem_type(base_type) rwork \
      ) { \
      int info = 0; \
      SOURCE(name)(&m, &n, &nrhs, \
                   CAST_ ## mem_type(a), \
                   &lda, \
                   CAST_ ## mem_type(b), \
                   &ldb, \
                   CAST_ ## mem_type(jpvt), \
                   &rcond, \
                   &rank, \
                   CAST_ ## mem_type(work), \
                   &lwork, \
                   CAST_ ## mem_type(rwork), \
                   &info); \
      LAPACK_ERROR(__FUNCTION__, "Unspecified error", info); \
   } \
   inline void base( \
                     size_t const &s_m, \
                     size_t const &s_n, \
                     size_t const &s_nrhs, \
                     TYPE_ ## mem_type(data_type) a, \
                     size_t const &s_lda, \
                     TYPE_ ## mem_type(data_type) b, \
                     size_t const &s_ldb, \
                     TYPE_ ## mem_type(int) jpvt, \
                     base_type const &rcond, \
                     size_t &s_rank, \
                     TYPE_ ## mem_type(data_type) work, \
                     size_t const &s_lwork, \
                     TYPE_ ## mem_type(base_type) rwork \
   ) { \
      int const m = static_cast<int const>(s_m); \
      int const n = static_cast<int const>(s_n); \
      int const nrhs = static_cast<int const>(s_nrhs); \
      int const lda = static_cast<int const>(s_lda); \
      int const ldb = static_cast<int const>(s_ldb); \
      int const lwork = static_cast<int const>(s_lwork); \
      int rank = static_cast<int>(s_rank); \
      int info = 0; \
      SOURCE(name)(&m, &n, &nrhs, \
                   CAST_ ## mem_type(a), \
                   &lda, \
                   CAST_ ## mem_type(b), \
                   &ldb, \
                   CAST_ ## mem_type(jpvt), \
                   &rcond, \
                   &rank, \
                   CAST_ ## mem_type(work), \
                   &lwork, \
                   CAST_ ## mem_type(rwork), \
                   &info); \
      LAPACK_ERROR(__FUNCTION__, "Unspecified error", info); \
      s_rank = rank; \
   }

BUILD_COMPLEX(gelsy)


#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                     int const &m, \
                     int const &n, \
                     int const &nrhs, \
                     TYPE_ ## mem_type(data_type) a, \
                     int const &lda, \
                     TYPE_ ## mem_type(data_type) b, \
                     int const &ldb, \
                     TYPE_ ## mem_type(int) jpvt, \
                     base_type const &rcond, \
                     int &rank, \
                     TYPE_ ## mem_type(data_type) work, \
                     int const &lwork \
      ) { \
      int info = 0; \
      SOURCE(name)(&m, &n, &nrhs, \
                   CAST_ ## mem_type(a), \
                   &lda, \
                   CAST_ ## mem_type(b), \
                   &ldb, \
                   CAST_ ## mem_type(jpvt), \
                   &rcond, \
                   &rank, \
                   CAST_ ## mem_type(work), \
                   &lwork, \
                   &info); \
      LAPACK_ERROR(__FUNCTION__, "Unspecified error", info); \
   } \
   inline void base( \
                     size_t const &s_m, \
                     size_t const &s_n, \
                     size_t const &s_nrhs, \
                     TYPE_ ## mem_type(data_type) a, \
                     size_t const &s_lda, \
                     TYPE_ ## mem_type(data_type) b, \
                     size_t const &s_ldb, \
                     TYPE_ ## mem_type(int) jpvt, \
                     base_type const &rcond, \
                     size_t &s_rank, \
                     TYPE_ ## mem_type(data_type) work, \
                     size_t const &s_lwork \
   ) { \
      int const m = static_cast<int const>(s_m); \
      int const n = static_cast<int const>(s_n); \
      int const nrhs = static_cast<int const>(s_nrhs); \
      int const lda = static_cast<int const>(s_lda); \
      int const ldb = static_cast<int const>(s_ldb); \
      int const lwork = static_cast<int const>(s_lwork); \
      int rank = static_cast<int>(s_rank); \
      int info = 0; \
      SOURCE(name)(&m, &n, &nrhs, \
                   CAST_ ## mem_type(a), \
                   &lda, \
                   CAST_ ## mem_type(b), \
                   &ldb, \
                   CAST_ ## mem_type(jpvt), \
                   &rcond, \
                   &rank, \
                   CAST_ ## mem_type(work), \
                   &lwork, \
                   &info); \
      LAPACK_ERROR(__FUNCTION__, "Unspecified error", info); \
      s_rank = rank; \
   }

BUILD_REAL(gelsy)

   /*
    *  GELSS computes the minimum norm solution to a complex linear
    *  least squares problem:
    *
    *  Minimize 2-norm(| b - A*x |).
    *
    *  using the singular value decomposition (SVD) of A. A is an M-by-N
    *  matrix which may be rank-deficient.
    *
    *  Several right hand side vectors b and solution vectors x can be
    *  handled in a single call; they are stored as the columns of the
    *  M-by-NRHS right hand side matrix B and the N-by-NRHS solution matrix
    *  X.
    *
    *  The effective rank of A is determined by treating as zero those
    *  singular values which are less than RCOND times the largest singular
    *  value.
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                     int const &m, \
                     int const &n, \
                     int const &nrhs, \
                     TYPE_ ## mem_type(data_type) a, \
                     int const &lda, \
                     TYPE_ ## mem_type(data_type) b, \
                     int const &ldb, \
                     TYPE_ ## mem_type(base_type) s, \
                     base_type const &rcond, \
                     int &rank, \
                     TYPE_ ## mem_type(data_type) work, \
                     int const &lwork, \
                     TYPE_ ## mem_type(base_type) rwork \
      ) { \
      int info = 0; \
      SOURCE(name)(&m, &n, &nrhs, \
                   CAST_ ## mem_type(a), \
                   &lda, \
                   CAST_ ## mem_type(b), \
                   &ldb, \
                   CAST_ ## mem_type(s), \
                   &rcond, \
                   &rank, \
                   CAST_ ## mem_type(work), \
                   &lwork, \
                   CAST_ ## mem_type(rwork), \
                   &info); \
      LAPACK_ERROR(__FUNCTION__, "Did not converge to zero", info); \
   } \
   inline void base( \
                     size_t const &s_m, \
                     size_t const &s_n, \
                     size_t const &s_nrhs, \
                     TYPE_ ## mem_type(data_type) a, \
                     size_t const &s_lda, \
                     TYPE_ ## mem_type(data_type) b, \
                     size_t const &s_ldb, \
                     TYPE_ ## mem_type(base_type) s, \
                     base_type const &rcond, \
                     size_t &s_rank, \
                     TYPE_ ## mem_type(data_type) work, \
                     size_t const &s_lwork, \
                     TYPE_ ## mem_type(base_type) rwork \
   ) { \
      int const m = static_cast<int const>(s_m); \
      int const n = static_cast<int const>(s_n); \
      int const nrhs = static_cast<int const>(s_nrhs); \
      int const lda = static_cast<int const>(s_lda); \
      int const ldb = static_cast<int const>(s_ldb); \
      int const lwork = static_cast<int const>(s_lwork); \
      int rank = static_cast<int>(s_rank); \
      int info = 0; \
      SOURCE(name)(&m, &n, &nrhs, \
                   CAST_ ## mem_type(a), \
                   &lda, \
                   CAST_ ## mem_type(b), \
                   &ldb, \
                   CAST_ ## mem_type(s), \
                   &rcond, \
                   &rank, \
                   CAST_ ## mem_type(work), \
                   &lwork, \
                   CAST_ ## mem_type(rwork), \
                   &info); \
      LAPACK_ERROR(__FUNCTION__, "Did not converge to zero", info); \
      s_rank = rank; \
   }

BUILD_COMPLEX(gelss)

#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                     int const &m, \
                     int const &n, \
                     int const &nrhs, \
                     TYPE_ ## mem_type(data_type) a, \
                     int const &lda, \
                     TYPE_ ## mem_type(data_type) b, \
                     int const &ldb, \
                     TYPE_ ## mem_type(base_type) s, \
                     base_type const &rcond, \
                     int &rank, \
                     TYPE_ ## mem_type(data_type) work, \
                     int const &lwork \
      ) { \
      int info = 0; \
      SOURCE(name)(&m, &n, &nrhs, \
                   CAST_ ## mem_type(a), \
                   &lda, \
                   CAST_ ## mem_type(b), \
                   &ldb, \
                   CAST_ ## mem_type(s), \
                   &rcond, \
                   &rank, \
                   CAST_ ## mem_type(work), \
                   &lwork, \
                   &info); \
      LAPACK_ERROR(__FUNCTION__, "Did not converge to zero", info); \
   } \
   inline void base( \
                     size_t const &s_m, \
                     size_t const &s_n, \
                     size_t const &s_nrhs, \
                     TYPE_ ## mem_type(data_type) a, \
                     size_t const &s_lda, \
                     TYPE_ ## mem_type(data_type) b, \
                     size_t const &s_ldb, \
                     TYPE_ ## mem_type(base_type) s, \
                     base_type const &rcond, \
                     size_t &s_rank, \
                     TYPE_ ## mem_type(data_type) work, \
                     size_t const &s_lwork \
   ) { \
      int const m = static_cast<int const>(s_m); \
      int const n = static_cast<int const>(s_n); \
      int const nrhs = static_cast<int const>(s_nrhs); \
      int const lda = static_cast<int const>(s_lda); \
      int const ldb = static_cast<int const>(s_ldb); \
      int const lwork = static_cast<int const>(s_lwork); \
      int rank = static_cast<int>(s_rank); \
      int info = 0; \
      SOURCE(name)(&m, &n, &nrhs, \
                   CAST_ ## mem_type(a), \
                   &lda, \
                   CAST_ ## mem_type(b), \
                   &ldb, \
                   CAST_ ## mem_type(s), \
                   &rcond, \
                   &rank, \
                   CAST_ ## mem_type(work), \
                   &lwork, \
                   &info); \
      LAPACK_ERROR(__FUNCTION__, "Did not converge to zero", info); \
      s_rank = rank; \
   }

BUILD_REAL(gelss)


   /*
    *  ZGELSD computes the minimum-norm solution to a real linear least
    *  squares problem:
    *      minimize 2-norm(| b - A*x |)
    *  using the singular value decomposition (SVD) of A. A is an M-by-N
    *  matrix which may be rank-deficient.
    *
    *  Several right hand side vectors b and solution vectors x can be
    *  handled in a single call; they are stored as the columns of the
    *  M-by-NRHS right hand side matrix B and the N-by-NRHS solution
    *  matrix X.
    *
    *  The problem is solved in three steps:
    *  (1) Reduce the coefficient matrix A to bidiagonal form with
    *      Householder tranformations, reducing the original problem
    *      into a "bidiagonal least squares problem" (BLS)
    *  (2) Solve the BLS using a divide and conquer approach.
    *  (3) Apply back all the Householder tranformations to solve
    *      the original least squares problem.
    *
    *  The effective rank of A is determined by treating as zero those
    *  singular values which are less than RCOND times the largest singular
    *  value.
    *
    *  The divide and conquer algorithm makes very mild assumptions about
    *  floating point arithmetic. It will work on machines with a guard
    *  digit in add/subtract, or on those binary machines without guard
    *  digits which subtract like the Cray X-MP, Cray Y-MP, Cray C-90, or
    *  Cray-2. It could conceivably fail on hexadecimal or decimal machines
    *  without guard digits, but we know of none.
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                     int const &m, \
                     int const &n, \
                     int const &nrhs, \
                     TYPE_ ## mem_type(data_type) a, \
                     int const &lda, \
                     TYPE_ ## mem_type(data_type) b, \
                     int const &ldb, \
                     TYPE_ ## mem_type(base_type) s, \
                     base_type const &rcond, \
                     int &rank, \
                     TYPE_ ## mem_type(data_type) work, \
                     int const &lwork, \
                     TYPE_ ## mem_type(base_type) rwork, \
                     TYPE_ ## mem_type(int) iwork \
      ) { \
      int info = 0; \
      SOURCE(name)(&m, &n, &nrhs, \
                   CAST_ ## mem_type(a), \
                   &lda, \
                   CAST_ ## mem_type(b), \
                   &ldb, \
                   CAST_ ## mem_type(s), \
                   &rcond, \
                   &rank, \
                   CAST_ ## mem_type(work), \
                   &lwork, \
                   CAST_ ## mem_type(rwork), \
                   CAST_ ## mem_type(iwork), \
                   &info); \
      LAPACK_ERROR(__FUNCTION__, "Did not converge to zero", info); \
   } \
   inline void base( \
                     size_t const &s_m, \
                     size_t const &s_n, \
                     size_t const &s_nrhs, \
                     TYPE_ ## mem_type(data_type) a, \
                     size_t const &s_lda, \
                     TYPE_ ## mem_type(data_type) b, \
                     size_t const &s_ldb, \
                     TYPE_ ## mem_type(base_type) s, \
                     base_type const &rcond, \
                     size_t &s_rank, \
                     TYPE_ ## mem_type(data_type) work, \
                     size_t const &s_lwork, \
                     TYPE_ ## mem_type(base_type) rwork, \
                     TYPE_ ## mem_type(int) iwork \
   ) { \
      int const m = static_cast<int const>(s_m); \
      int const n = static_cast<int const>(s_n); \
      int const nrhs = static_cast<int const>(s_nrhs); \
      int const lda = static_cast<int const>(s_lda); \
      int const ldb = static_cast<int const>(s_ldb); \
      int const lwork = static_cast<int const>(s_lwork); \
      int rank = static_cast<int>(s_rank); \
      int info = 0; \
      SOURCE(name)(&m, &n, &nrhs, \
                   CAST_ ## mem_type(a), \
                   &lda, \
                   CAST_ ## mem_type(b), \
                   &ldb, \
                   CAST_ ## mem_type(s), \
                   &rcond, \
                   &rank, \
                   CAST_ ## mem_type(work), \
                   &lwork, \
                   CAST_ ## mem_type(rwork), \
                   CAST_ ## mem_type(iwork), \
                   &info); \
      LAPACK_ERROR(__FUNCTION__, "Did not converge to zero", info); \
      s_rank = rank; \
   }

BUILD_COMPLEX(gelsd)

#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                     int const &m, \
                     int const &n, \
                     int const &nrhs, \
                     TYPE_ ## mem_type(data_type) a, \
                     int const &lda, \
                     TYPE_ ## mem_type(data_type) b, \
                     int const &ldb, \
                     TYPE_ ## mem_type(base_type) s, \
                     base_type const &rcond, \
                     int &rank, \
                     TYPE_ ## mem_type(data_type) work, \
                     int const &lwork, \
                     TYPE_ ## mem_type(int) iwork \
      ) { \
      int info = 0; \
      SOURCE(name)(&m, &n, &nrhs, \
                   CAST_ ## mem_type(a), \
                   &lda, \
                   CAST_ ## mem_type(b), \
                   &ldb, \
                   CAST_ ## mem_type(s), \
                   &rcond, \
                   &rank, \
                   CAST_ ## mem_type(work), \
                   &lwork, \
                   CAST_ ## mem_type(iwork), \
                   &info); \
      LAPACK_ERROR(__FUNCTION__, "Did not converge to zero", info); \
   } \
   inline void base( \
                     size_t const &s_m, \
                     size_t const &s_n, \
                     size_t const &s_nrhs, \
                     TYPE_ ## mem_type(data_type) a, \
                     size_t const &s_lda, \
                     TYPE_ ## mem_type(data_type) b, \
                     size_t const &s_ldb, \
                     TYPE_ ## mem_type(base_type) s, \
                     base_type const &rcond, \
                     size_t &s_rank, \
                     TYPE_ ## mem_type(data_type) work, \
                     size_t const &s_lwork, \
                     TYPE_ ## mem_type(int) iwork \
   ) { \
      int const m = static_cast<int const>(s_m); \
      int const n = static_cast<int const>(s_n); \
      int const nrhs = static_cast<int const>(s_nrhs); \
      int const lda = static_cast<int const>(s_lda); \
      int const ldb = static_cast<int const>(s_ldb); \
      int const lwork = static_cast<int const>(s_lwork); \
      int rank = static_cast<int>(s_rank); \
      int info = 0; \
      SOURCE(name)(&m, &n, &nrhs, \
                   CAST_ ## mem_type(a), \
                   &lda, \
                   CAST_ ## mem_type(b), \
                   &ldb, \
                   CAST_ ## mem_type(s), \
                   &rcond, \
                   &rank, \
                   CAST_ ## mem_type(work), \
                   &lwork, \
                   CAST_ ## mem_type(iwork), \
                   &info); \
      LAPACK_ERROR(__FUNCTION__, "Did not converge to zero", info); \
      s_rank = rank; \
   }

BUILD_REAL(gelsd)


   /*
    * ZGESV computes the solution to a complex system of linear equations
    *    A * X = B,
    * where A is an N-by-N matrix and X and B are N-by-NRHS matrices.
    *
    * The LU decomposition with partial pivoting and row interchanges is
    * used to factor A as
    *    A = P * L * U,
    * where P is a permutation matrix, L is unit lower triangular, and U is
    * upper triangular.  The factored form of A is then used to solve the
    * system of equations A * X = B.
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                     int const &m, \
                     int const &n, \
                     TYPE_ ## mem_type(data_type) a, \
                     int const &lda, \
                     TYPE_ ## mem_type(int) ipiv, \
                     TYPE_ ## mem_type(data_type) b, \
                     int const &ldb \
      ) { \
      int info = 0; \
      SOURCE(name)(&m, &n, \
                   CAST_ ## mem_type(a), \
                   &lda, \
                   CAST_ ## mem_type(ipiv), \
                   CAST_ ## mem_type(b), \
                   &ldb, \
                   &info); \
      LAPACK_ERROR(__FUNCTION__, "Matrix is singular", info); \
   } \
   inline void base( \
                     size_t const &s_m, \
                     size_t const &s_n, \
                     TYPE_ ## mem_type(data_type) a, \
                     size_t const &s_lda, \
                     TYPE_ ## mem_type(int) ipiv, \
                     TYPE_ ## mem_type(data_type) b, \
                     size_t const &s_ldb \
   ) { \
      int const m = static_cast<int const>(s_m); \
      int const n = static_cast<int const>(s_n); \
      int const lda = static_cast<int const>(s_lda); \
      int const ldb = static_cast<int const>(s_ldb); \
      int info = 0; \
      SOURCE(name)(&m, &n, \
                   CAST_ ## mem_type(a), \
                   &lda, \
                   CAST_ ## mem_type(ipiv), \
                   CAST_ ## mem_type(b), \
                   &ldb, \
                   &info); \
      LAPACK_ERROR(__FUNCTION__, "Matrix is singular", info); \
   }

BUILD_ALL(gesv)


   /*
    * DPOSV computes the solution to a real system of linear equations
    *    A * X = B,
    * where A is an N-by-N symmetric positive definite matrix and X and B
    * are N-by-NRHS matrices.
    *
    * The Cholesky decomposition is used to factor A as
    *    A = U**T* U,  if UPLO = 'U', or
    *    A = L * L**T,  if UPLO = 'L',
    * where U is an upper triangular matrix and L is a lower triangular
    * matrix.  The factored form of A is then used to solve the system of
    * equations A * X = B.
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                     char const &uplo, \
                     int const &n, \
                     int const &nrhs, \
                     TYPE_ ## mem_type(data_type) a, \
                     int const &lda, \
                     TYPE_ ## mem_type(data_type) b, \
                     int const &ldb \
      ) { \
      int info = 0; \
      SOURCE(name)(&uplo, &n, &nrhs,  \
                   CAST_ ## mem_type(a), \
                   &lda, \
                   CAST_ ## mem_type(b), \
                   &ldb, \
                   &info); \
      LAPACK_ERROR(__FUNCTION__, "Leading order is not positive definite", info); \
   } \
   inline void base( \
                     char const &uplo, \
                     size_t const &s_n, \
                     size_t const &s_nrhs, \
                     TYPE_ ## mem_type(data_type) a, \
                     size_t const &s_lda, \
                     TYPE_ ## mem_type(data_type) b, \
                     size_t const &s_ldb \
   ) { \
      int const n = static_cast<int const>(s_n); \
      int const nrhs = static_cast<int const>(s_nrhs); \
      int const lda = static_cast<int const>(s_lda); \
      int const ldb = static_cast<int const>(s_ldb); \
      int info = 0; \
      SOURCE(name)(&uplo, &n, &nrhs, \
                   CAST_ ## mem_type(a), \
                   &lda, \
                   CAST_ ## mem_type(b), \
                   &ldb, \
                   &info); \
      LAPACK_ERROR(__FUNCTION__, "Leading order is not positive definite", info); \
   }

BUILD_ALL(posv)


   /*
    *  ZHEEVR computes selected eigenvalues and, optionally, eigenvectors
    *  of a complex Hermitian matrix A.  Eigenvalues and eigenvectors can
    *  be selected by specifying either a range of values or a range of
    *  indices for the desired eigenvalues.
    *
    *  ZHEEVR first reduces the matrix A to tridiagonal form T with a call
    *  to ZHETRD.  Then, whenever possible, ZHEEVR calls ZSTEMR to compute
    *  eigenspectrum using Relatively Robust Representations.  ZSTEMR
    *  computes eigenvalues by the dqds algorithm, while orthogonal
    *  eigenvectors are computed from various "good" L D L^T representations
    *  (also known as Relatively Robust Representations). Gram-Schmidt
    *  orthogonalization is avoided as far as possible. More specifically,
    *  the various steps of the algorithm are as follows.
    *
    *  For each unreduced block (submatrix) of T,
    *     (a) Compute T - sigma I  = L D L^T, so that L and D
    *         define all the wanted eigenvalues to high relative accuracy.
    *         This means that small relative changes in the entries of D and L
    *         cause only small relative changes in the eigenvalues and
    *         eigenvectors. The standard (unfactored) representation of the
    *         tridiagonal matrix T does not have this property in general.
    *     (b) Compute the eigenvalues to suitable accuracy.
    *         If the eigenvectors are desired, the algorithm attains full
    *         accuracy of the computed eigenvalues only right before
    *         the corresponding vectors have to be computed, see steps c) and d).
    *     (c) For each cluster of close eigenvalues, select a new
    *         shift close to the cluster, find a new factorization, and refine
    *         the shifted eigenvalues to suitable accuracy.
    *     (d) For each eigenvalue with a large enough relative separation compute
    *         the corresponding eigenvector by forming a rank revealing twisted
    *         factorization. Go back to (c) for any clusters that remain.
    *
    *  The desired accuracy of the output can be specified by the input
    *  parameter ABSTOL.
    *
    *  For more details, see DSTEMR's documentation and:
    *  - Inderjit S. Dhillon and Beresford N. Parlett: "Multiple representations
    *    to compute orthogonal eigenvectors of symmetric tridiagonal matrices,"
    *    Linear Algebra and its Applications, 387(1), pp. 1-28, August 2004.
    *  - Inderjit Dhillon and Beresford Parlett: "Orthogonal Eigenvectors and
    *    Relative Gaps," SIAM Journal on Matrix Analysis and Applications, Vol. 25,
    *    2004.  Also LAPACK Working Note 154.
    *  - Inderjit Dhillon: "A new O(n^2) algorithm for the symmetric
    *    tridiagonal eigenvalue/eigenvector problem",
    *    Computer Science Division Technical Report No. UCB/CSD-97-971,
    *    UC Berkeley, May 1997.
    *
    *
    *  Note 1 : ZHEEVR calls ZSTEMR when the full spectrum is requested
    *  on machines which conform to the ieee-754 floating point standard.
    *  ZHEEVR calls DSTEBZ and ZSTEIN on non-ieee machines and
    *  when partial spectrum requests are made.
    *
    *  Normal execution of ZSTEMR may create NaNs and infinities and
    *  hence may abort due to a floating point exception in environments
    *  which do not handle NaNs and infinities in the ieee standard default
    *  manner.
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                     char const &jobz \
                   , char const &range \
                   , char const &uplo \
                   , int const &n \
                   , TYPE_ ## mem_type(data_type) a \
                   , int const &lda \
                   , base_type const &vl \
                   , base_type const &vu \
                   , int const &il \
                   , int const &iu \
                   , base_type const &abstol \
                   , int &m \
                   , TYPE_ ## mem_type(base_type) w \
                   , TYPE_ ## mem_type(data_type) z \
                   , int const &ldz \
                   , TYPE_ ## mem_type(int) iSuppZ \
                   , TYPE_ ## mem_type(data_type) work \
                   , int const &lwork \
                   , TYPE_ ## mem_type(base_type) rwork  \
                   , int const &lrwork  \
                   , TYPE_ ## mem_type(int) iwork  \
                   , int const &liwork  \
      ) { \
      int info = 0; \
      SOURCE(name)(&jobz, &range, &uplo, &n  \
                ,  CAST_ ## mem_type(a) \
                ,  &lda, &vl, &vu, &il, &iu, &abstol, &m \
                ,  CAST_ ## mem_type(w) \
                ,  CAST_ ## mem_type(z), &ldz \
                ,  CAST_ ## mem_type(iSuppZ) \
                ,  CAST_ ## mem_type(work), &lwork \
                ,  CAST_ ## mem_type(rwork), &lrwork \
                ,  CAST_ ## mem_type(iwork), &liwork \
                ,  &info); \
      LAPACK_ERROR(__FUNCTION__, "Internal error", info); \
   } \
   inline void base( \
                     char const &jobz \
                   , char const &range \
                   , char const &uplo \
                   , size_t const &s_n \
                   , TYPE_ ## mem_type(data_type) a \
                   , size_t const &s_lda \
                   , base_type const &vl \
                   , base_type const &vu \
                   , size_t const &s_il \
                   , size_t const &s_iu \
                   , base_type const &abstol \
                   , size_t &s_m \
                   , TYPE_ ## mem_type(base_type) w \
                   , TYPE_ ## mem_type(data_type) z \
                   , size_t const &s_ldz \
                   , TYPE_ ## mem_type(int) iSuppZ \
                   , TYPE_ ## mem_type(data_type) work \
                   , size_t const &s_lwork \
                   , TYPE_ ## mem_type(base_type) rwork  \
                   , size_t const &s_lrwork  \
                   , TYPE_ ## mem_type(int) iwork  \
                   , size_t const &s_liwork  \
   ) { \
      int const n = static_cast<int const>(s_n); \
      int const m = static_cast<int const>(s_m); \
      int const lda = static_cast<int const>(s_lda); \
      int const ldz = static_cast<int const>(s_ldz); \
      int const il = static_cast<int const>(s_il); \
      int const iu = static_cast<int const>(s_iu); \
      int const lwork = static_cast<int const>(s_lwork); \
      int const liwork = static_cast<int const>(s_liwork); \
      int const lrwork = static_cast<int const>(s_lrwork); \
      int info = 0; \
      SOURCE(name)(&jobz, &range, &uplo, &n  \
                ,  CAST_ ## mem_type(a) \
                ,  &lda, &vl, &vu, &il, &iu, &abstol, &m \
                ,  CAST_ ## mem_type(w) \
                ,  CAST_ ## mem_type(z), &ldz \
                ,  CAST_ ## mem_type(iSuppZ) \
                ,  CAST_ ## mem_type(work), &lwork \
                ,  CAST_ ## mem_type(rwork), &lrwork \
                ,  CAST_ ## mem_type(iwork), &liwork \
                ,  &info); \
      LAPACK_ERROR(__FUNCTION__, "Internal error", info); \
      s_m = m; \
   }

BUILD_COMPLEX(heevr)


   /*
    *  HEEVD computes all eigenvalues and, optionally, eigenvectors of a
    *  complex Hermitian matrix A.  If eigenvectors are desired, it uses a
    *  divide and conquer algorithm.
    *
    *  The divide and conquer algorithm makes very mild assumptions about
    *  floating point arithmetic. It will work on machines with a guard
    *  digit in add/subtract, or on those binary machines without guard
    *  digits which subtract like the Cray X-MP, Cray Y-MP, Cray C-90, or
    *  Cray-2. It could conceivably fail on hexadecimal or decimal machines
    *  without guard digits, but we know of none.
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                     char const &jobz \
                   , char const &uplo \
                   , int const &n \
                   , TYPE_ ## mem_type(data_type) a \
                   , int const &lda \
                   , TYPE_ ## mem_type(base_type) w \
                   , TYPE_ ## mem_type(data_type) work \
                   , int const &lwork \
                   , TYPE_ ## mem_type(base_type) rwork  \
                   , int const &lrwork  \
                   , TYPE_ ## mem_type(int) iwork  \
                   , int const &liwork  \
      ) { \
      int info = 0; \
      SOURCE(name)(&jobz, &uplo, &n  \
                ,  CAST_ ## mem_type(a) \
                ,  &lda \
                ,  CAST_ ## mem_type(w) \
                ,  CAST_ ## mem_type(work), &lwork \
                ,  CAST_ ## mem_type(rwork), &lrwork \
                ,  CAST_ ## mem_type(iwork), &liwork \
                ,  &info); \
      LAPACK_ERROR(__FUNCTION__, "Failed to converge", info); \
   } \
   inline void base( \
                     char const &jobz \
                   , char const &uplo \
                   , size_t const &s_n \
                   , TYPE_ ## mem_type(data_type) a \
                   , size_t const &s_lda \
                   , TYPE_ ## mem_type(base_type) w \
                   , TYPE_ ## mem_type(data_type) work \
                   , size_t const &s_lwork \
                   , TYPE_ ## mem_type(base_type) rwork  \
                   , size_t const &s_lrwork  \
                   , TYPE_ ## mem_type(int) iwork  \
                   , size_t const &s_liwork  \
   ) { \
      int const n = static_cast<int const>(s_n); \
      int const lda = static_cast<int const>(s_lda); \
      int const lwork = static_cast<int const>(s_lwork); \
      int const liwork = static_cast<int const>(s_liwork); \
      int const lrwork = static_cast<int const>(s_lrwork); \
      int info = 0; \
      SOURCE(name)(&jobz, &uplo, &n  \
                ,  CAST_ ## mem_type(a) \
                ,  &lda \
                ,  CAST_ ## mem_type(w) \
                ,  CAST_ ## mem_type(work), &lwork \
                ,  CAST_ ## mem_type(rwork), &lrwork \
                ,  CAST_ ## mem_type(iwork), &liwork \
                ,  &info); \
      LAPACK_ERROR(__FUNCTION__, "Failed to converge", info); \
   }

BUILD_COMPLEX(heevd)

   /*
    *  HEEV computes all eigenvalues and, optionally, eigenvectors of a
    *  complex Hermitian matrix A.
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                     char const &jobz \
                   , char const &uplo \
                   , int const &n \
                   , TYPE_ ## mem_type(data_type) a \
                   , int const &lda \
                   , TYPE_ ## mem_type(base_type) w \
                   , TYPE_ ## mem_type(data_type) work \
                   , int const &lwork \
                   , TYPE_ ## mem_type(base_type) rwork  \
      ) { \
      int info = 0; \
      SOURCE(name)(&jobz, &uplo, &n  \
                ,  CAST_ ## mem_type(a) \
                ,  &lda \
                ,  CAST_ ## mem_type(w) \
                ,  CAST_ ## mem_type(work), &lwork \
                ,  CAST_ ## mem_type(rwork) \
                ,  &info); \
      LAPACK_ERROR(__FUNCTION__, "Failed to converge", info); \
   } \
   inline void base( \
                     char const &jobz \
                   , char const &uplo \
                   , size_t const &s_n \
                   , TYPE_ ## mem_type(data_type) a \
                   , size_t const &s_lda \
                   , TYPE_ ## mem_type(base_type) w \
                   , TYPE_ ## mem_type(data_type) work \
                   , size_t const &s_lwork \
                   , TYPE_ ## mem_type(base_type) rwork  \
   ) { \
      int const n = static_cast<int const>(s_n); \
      int const lda = static_cast<int const>(s_lda); \
      int const lwork = static_cast<int const>(s_lwork); \
      int info = 0; \
      SOURCE(name)(&jobz, &uplo, &n  \
                ,  CAST_ ## mem_type(a) \
                ,  &lda \
                ,  CAST_ ## mem_type(w) \
                ,  CAST_ ## mem_type(work), &lwork \
                ,  CAST_ ## mem_type(rwork) \
                ,  &info); \
      LAPACK_ERROR(__FUNCTION__, "Failed to converge", info); \
   }

BUILD_COMPLEX(heev)


   /*
    *  GEQRF computes a QR factorization of a complex M-by-N matrix A:
    *  A = Q * R.
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                     int const &m \
                   , int const &n \
                   , TYPE_ ## mem_type(data_type) a \
                   , int const &lda \
                   , TYPE_ ## mem_type(data_type) tau \
                   , TYPE_ ## mem_type(data_type) work \
                   , int const &lwork \
      ) { \
      int info = 0; \
      SOURCE(name)(&m, &n  \
                ,  CAST_ ## mem_type(a) \
                ,  &lda \
                ,  CAST_ ## mem_type(tau) \
                ,  CAST_ ## mem_type(work), &lwork \
                ,  &info); \
      LAPACK_ERROR(__FUNCTION__, "Unknown error", info); \
   } \
   inline void base( \
                     size_t const &s_m \
                   , size_t const &s_n \
                   , TYPE_ ## mem_type(data_type) a \
                   , size_t const &s_lda \
                   , TYPE_ ## mem_type(data_type) tau \
                   , TYPE_ ## mem_type(data_type) work \
                   , size_t const &s_lwork \
   ) { \
      int const m = static_cast<int const>(s_m); \
      int const n = static_cast<int const>(s_n); \
      int const lda = static_cast<int const>(s_lda); \
      int const lwork = static_cast<int const>(s_lwork); \
      int info = 0; \
      SOURCE(name)(&m, &n  \
                ,  CAST_ ## mem_type(a) \
                ,  &lda \
                ,  CAST_ ## mem_type(tau) \
                ,  CAST_ ## mem_type(work), &lwork \
                ,  &info); \
      LAPACK_ERROR(__FUNCTION__, "Unknown error", info); \
   }

BUILD_ALL(geqrf)

#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                     int const &m \
                   , int const &n \
                   , TYPE_ ## mem_type(data_type) a \
                   , int const &lda \
                   , TYPE_ ## mem_type(data_type) tau \
                   , TYPE_ ## mem_type(data_type) work \
      ) { \
      int info = 0; \
      int const lwork = static_cast<int const>(work.size()); \
      SOURCE(name)(&m, &n  \
                ,  CAST_ ## mem_type(a) \
                ,  &lda \
                ,  CAST_ ## mem_type(tau) \
                ,  CAST_ ## mem_type(work), &lwork \
                ,  &info); \
      LAPACK_ERROR(__FUNCTION__, "Unknown error", info); \
   } \
   inline void base( \
                     size_t const &s_m \
                   , size_t const &s_n \
                   , TYPE_ ## mem_type(data_type) a \
                   , size_t const &s_lda \
                   , TYPE_ ## mem_type(data_type) tau \
                   , TYPE_ ## mem_type(data_type) work \
   ) { \
      int const m = static_cast<int const>(s_m); \
      int const n = static_cast<int const>(s_n); \
      int const lda = static_cast<int const>(s_lda); \
      int const lwork = static_cast<int const>(work.size()); \
      int info = 0; \
      SOURCE(name)(&m, &n  \
                ,  CAST_ ## mem_type(a) \
                ,  &lda \
                ,  CAST_ ## mem_type(tau) \
                ,  CAST_ ## mem_type(work), &lwork \
                ,  &info); \
      LAPACK_ERROR(__FUNCTION__, "Unknown error", info); \
   }

BUILD_ALL_VECTOR(geqrf)


    /*
    *  GEQP3 computes a QR factorization with column pivoting of a
    *  matrix A:  A*P = Q*R  using Level 3 BLAS.
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                     int const &m \
                   , int const &n \
                   , TYPE_ ## mem_type(data_type) a \
                   , int const &lda \
                   , TYPE_ ## mem_type(int) jpvt\
                   , TYPE_ ## mem_type(data_type) tau \
                   , TYPE_ ## mem_type(data_type) work \
                   , int const &lwork \
                   , TYPE_ ## mem_type(base_type) rwork \
      ) { \
      int info = 0; \
      SOURCE(name)(&m, &n  \
                ,  CAST_ ## mem_type(a) \
                ,  &lda \
                ,  CAST_ ## mem_type(jpvt) \
                ,  CAST_ ## mem_type(tau) \
                ,  CAST_ ## mem_type(work), &lwork \
                ,  CAST_ ## mem_type(rwork) \
                ,  &info); \
      LAPACK_ERROR(__FUNCTION__, "Unknown error", info); \
   } \
   inline void base( \
                     size_t const &s_m \
                   , size_t const &s_n \
                   , TYPE_ ## mem_type(data_type) a \
                   , size_t const &s_lda \
                   , TYPE_ ## mem_type(int) jpvt\
                   , TYPE_ ## mem_type(data_type) tau \
                   , TYPE_ ## mem_type(data_type) work \
                   , size_t const &s_lwork \
                   , TYPE_ ## mem_type(base_type) rwork \
   ) { \
      int const m = static_cast<int const>(s_m); \
      int const n = static_cast<int const>(s_n); \
      int const lda = static_cast<int const>(s_lda); \
      int const lwork = static_cast<int const>(s_lwork); \
      int info = 0; \
      SOURCE(name)(&m, &n  \
                ,  CAST_ ## mem_type(a) \
                ,  &lda \
                ,  CAST_ ## mem_type(jpvt) \
                ,  CAST_ ## mem_type(tau) \
                ,  CAST_ ## mem_type(work), &lwork \
                ,  CAST_ ## mem_type(rwork) \
                ,  &info); \
      LAPACK_ERROR(__FUNCTION__, "Unknown error", info); \
   }

BUILD_COMPLEX(geqp3)

#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                     int const &m \
                   , int const &n \
                   , TYPE_ ## mem_type(data_type) a \
                   , int const &lda \
                   , TYPE_ ## mem_type(int) jpvt\
                   , TYPE_ ## mem_type(data_type) tau \
                   , TYPE_ ## mem_type(data_type) work \
                   , int const &lwork \
      ) { \
      int info = 0; \
      SOURCE(name)(&m, &n  \
                ,  CAST_ ## mem_type(a) \
                ,  &lda \
                ,  CAST_ ## mem_type(jpvt) \
                ,  CAST_ ## mem_type(tau) \
                ,  CAST_ ## mem_type(work), &lwork \
                ,  &info); \
      LAPACK_ERROR(__FUNCTION__, "Unknown error", info); \
   } \
   inline void base( \
                     size_t const &s_m \
                   , size_t const &s_n \
                   , TYPE_ ## mem_type(data_type) a \
                   , size_t const &s_lda \
                   , TYPE_ ## mem_type(int) jpvt\
                   , TYPE_ ## mem_type(data_type) tau \
                   , TYPE_ ## mem_type(data_type) work \
                   , size_t const &s_lwork \
   ) { \
      int const m = static_cast<int const>(s_m); \
      int const n = static_cast<int const>(s_n); \
      int const lda = static_cast<int const>(s_lda); \
      int const lwork = static_cast<int const>(s_lwork); \
      int info = 0; \
      SOURCE(name)(&m, &n  \
                ,  CAST_ ## mem_type(a) \
                ,  &lda \
                ,  CAST_ ## mem_type(jpvt) \
                ,  CAST_ ## mem_type(tau) \
                ,  CAST_ ## mem_type(work), &lwork \
                ,  &info); \
      LAPACK_ERROR(__FUNCTION__, "Unknown error", info); \
   }

BUILD_REAL(geqp3)


   /*
    *  GELQF computes an LQ factorization of a M-by-N matrix A:
    *  A = L * Q.
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                     int const &m \
                   , int const &n \
                   , TYPE_ ## mem_type(data_type) a \
                   , int const &lda \
                   , TYPE_ ## mem_type(data_type) tau \
                   , TYPE_ ## mem_type(data_type) work \
                   , int const &lwork \
      ) { \
      int info = 0; \
      SOURCE(name)(&m, &n  \
                ,  CAST_ ## mem_type(a) \
                ,  &lda \
                ,  CAST_ ## mem_type(tau) \
                ,  CAST_ ## mem_type(work), &lwork \
                ,  &info); \
      LAPACK_ERROR(__FUNCTION__, "Unknown error", info); \
   } \
   inline void base( \
                     size_t const &s_m \
                   , size_t const &s_n \
                   , TYPE_ ## mem_type(data_type) a \
                   , size_t const &s_lda \
                   , TYPE_ ## mem_type(data_type) tau \
                   , TYPE_ ## mem_type(data_type) work \
                   , size_t const &s_lwork \
   ) { \
      int const m = static_cast<int const>(s_m); \
      int const n = static_cast<int const>(s_n); \
      int const lda = static_cast<int const>(s_lda); \
      int const lwork = static_cast<int const>(s_lwork); \
      int info = 0; \
      SOURCE(name)(&m, &n  \
                ,  CAST_ ## mem_type(a) \
                ,  &lda \
                ,  CAST_ ## mem_type(tau) \
                ,  CAST_ ## mem_type(work), &lwork \
                ,  &info); \
      LAPACK_ERROR(__FUNCTION__, "Unknown error", info); \
   }

BUILD_ALL(gelqf)

#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                     int const &m \
                   , int const &n \
                   , TYPE_ ## mem_type(data_type) a \
                   , int const &lda \
                   , TYPE_ ## mem_type(data_type) tau \
                   , TYPE_ ## mem_type(data_type) work \
      ) { \
      int info = 0; \
      int const lwork = static_cast<int const>(work.size()); \
      SOURCE(name)(&m, &n  \
                ,  CAST_ ## mem_type(a) \
                ,  &lda \
                ,  CAST_ ## mem_type(tau) \
                ,  CAST_ ## mem_type(work), &lwork \
                ,  &info); \
      LAPACK_ERROR(__FUNCTION__, "Unknown error", info); \
   } \
   inline void base( \
                     size_t const &s_m \
                   , size_t const &s_n \
                   , TYPE_ ## mem_type(data_type) a \
                   , size_t const &s_lda \
                   , TYPE_ ## mem_type(data_type) tau \
                   , TYPE_ ## mem_type(data_type) work \
   ) { \
      int const m = static_cast<int const>(s_m); \
      int const n = static_cast<int const>(s_n); \
      int const lda = static_cast<int const>(s_lda); \
      int const lwork = static_cast<int const>(work.size()); \
      int info = 0; \
      SOURCE(name)(&m, &n  \
                ,  CAST_ ## mem_type(a) \
                ,  &lda \
                ,  CAST_ ## mem_type(tau) \
                ,  CAST_ ## mem_type(work), &lwork \
                ,  &info); \
      LAPACK_ERROR(__FUNCTION__, "Unknown error", info); \
   }

 BUILD_ALL_VECTOR(gelqf)


   /*
    *  UNMLQ overwrites the general M-by-N matrix C with
    *
    *                  SIDE = 'L'     SIDE = 'R'
    *  TRANS = 'N':      Q * C          C * Q
    *  TRANS = 'C':      Q**H * C       C * Q**H
    *
    *  where Q is a unitary matrix defined as the product of k
    *  elementary reflectors
    *
    *        Q = H(k)' . . . H(2)' H(1)'
    *
    *  as returned by GELQF. Q is of order M if SIDE = 'L' and of order N
    *  if SIDE = 'R'.
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                     char const &side \
                   , char const &trans \
                   , int const &m \
                   , int const &n \
                   , int const &k \
                   , TYPE_ ## mem_type(data_type) a \
                   , int const &lda \
                   , TYPE_ ## mem_type(data_type) tau \
                   , TYPE_ ## mem_type(data_type) c \
                   , int const &ldc \
                   , TYPE_ ## mem_type(data_type) work \
                   , int const &lwork \
      ) { \
      int info = 0; \
      SOURCE(name)( \
                   &side, &trans, &m, &n, &k  \
                ,  CAST_ ## mem_type(a),  &lda \
                ,  CAST_ ## mem_type(tau) \
                ,  CAST_ ## mem_type(c),  &ldc \
                ,  CAST_ ## mem_type(work), &lwork \
                ,  &info); \
      LAPACK_ERROR(__FUNCTION__, "Unknown error", info); \
   } \
   inline void base( \
                     char const &side \
                   , char const &trans \
                   , size_t const &s_m \
                   , size_t const &s_n \
                   , size_t const &s_k \
                   , TYPE_ ## mem_type(data_type) a \
                   , size_t const &s_lda \
                   , TYPE_ ## mem_type(data_type) tau \
                   , TYPE_ ## mem_type(data_type) c \
                   , size_t const &s_ldc \
                   , TYPE_ ## mem_type(data_type) work \
                   , size_t const &s_lwork \
   ) { \
      int const m = static_cast<int const>(s_m); \
      int const n = static_cast<int const>(s_n); \
      int const k = static_cast<int const>(s_k); \
      int const lda = static_cast<int const>(s_lda); \
      int const ldc = static_cast<int const>(s_ldc); \
      int const lwork = static_cast<int const>(s_lwork); \
      int info = 0; \
      SOURCE(name)( \
                   &side, &trans, &m, &n, &k  \
                ,  CAST_ ## mem_type(a),  &lda \
                ,  CAST_ ## mem_type(tau) \
                ,  CAST_ ## mem_type(c),  &ldc \
                ,  CAST_ ## mem_type(work), &lwork \
                ,  &info); \
      LAPACK_ERROR(__FUNCTION__, "Unknown error", info); \
   }

BUILD_COMPLEX(unmlq)

#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                     char const &side \
                   , char const &trans \
                   , int const &m \
                   , int const &n \
                   , int const &k \
                   , TYPE_ ## mem_type(data_type) a \
                   , int const &lda \
                   , TYPE_ ## mem_type(data_type) tau \
                   , TYPE_ ## mem_type(data_type) c \
                   , int const &ldc \
                   , TYPE_ ## mem_type(data_type) work \
      ) { \
      int info = 0; \
      int const lwork = static_cast<int const>(work.size()); \
      SOURCE(name)( \
                   &side, &trans, &m, &n, &k  \
                ,  CAST_ ## mem_type(a),  &lda \
                ,  CAST_ ## mem_type(tau) \
                ,  CAST_ ## mem_type(c),  &ldc \
                ,  CAST_ ## mem_type(work), &lwork \
                ,  &info); \
      LAPACK_ERROR(__FUNCTION__, "Unknown error", info); \
   } \
   inline void base( \
                     char const &side \
                   , char const &trans \
                   , size_t const &s_m \
                   , size_t const &s_n \
                   , size_t const &s_k \
                   , TYPE_ ## mem_type(data_type) a \
                   , size_t const &s_lda \
                   , TYPE_ ## mem_type(data_type) tau \
                   , TYPE_ ## mem_type(data_type) c \
                   , size_t const &s_ldc \
                   , TYPE_ ## mem_type(data_type) work \
   ) { \
      int const m = static_cast<int const>(s_m); \
      int const n = static_cast<int const>(s_n); \
      int const k = static_cast<int const>(s_k); \
      int const lda = static_cast<int const>(s_lda); \
      int const ldc = static_cast<int const>(s_ldc); \
      int const lwork = static_cast<int const>(work.size()); \
      int info = 0; \
      SOURCE(name)( \
                   &side, &trans, &m, &n, &k  \
                ,  CAST_ ## mem_type(a),  &lda \
                ,  CAST_ ## mem_type(tau) \
                ,  CAST_ ## mem_type(c),  &ldc \
                ,  CAST_ ## mem_type(work), &lwork \
                ,  &info); \
      LAPACK_ERROR(__FUNCTION__, "Unknown error", info); \
   }

BUILD_COMPLEX_VECTOR(unmlq)


   /*
    *  UNMQR overwrites the general M-by-N matrix C with
    *
    *                  SIDE = 'L'     SIDE = 'R'
    *  TRANS = 'N':      Q * C          C * Q
    *  TRANS = 'C':      Q**H * C       C * Q**H
    *
    *  where Q is a unitary matrix defined as the product of k
    *  elementary reflectors
    *
    *        Q = H(1) H(2) . . . H(k)
    *
    *  as returned by GEQRF. Q is of order M if SIDE = 'L' and of order N
    *  if SIDE = 'R'.
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                     char const &side \
                   , char const &trans \
                   , int const &m \
                   , int const &n \
                   , int const &k \
                   , TYPE_ ## mem_type(data_type) a \
                   , int const &lda \
                   , TYPE_ ## mem_type(data_type) tau \
                   , TYPE_ ## mem_type(data_type) c \
                   , int const &ldc \
                   , TYPE_ ## mem_type(data_type) work \
                   , int const &lwork \
      ) { \
      int info = 0; \
      SOURCE(name)( \
                   &side, &trans, &m, &n, &k  \
                ,  CAST_ ## mem_type(a),  &lda \
                ,  CAST_ ## mem_type(tau) \
                ,  CAST_ ## mem_type(c),  &ldc \
                ,  CAST_ ## mem_type(work), &lwork \
                ,  &info); \
      LAPACK_ERROR(__FUNCTION__, "Unknown error", info); \
   } \
   inline void base( \
                     char const &side \
                   , char const &trans \
                   , size_t const &s_m \
                   , size_t const &s_n \
                   , size_t const &s_k \
                   , TYPE_ ## mem_type(data_type) a \
                   , size_t const &s_lda \
                   , TYPE_ ## mem_type(data_type) tau \
                   , TYPE_ ## mem_type(data_type) c \
                   , size_t const &s_ldc \
                   , TYPE_ ## mem_type(data_type) work \
                   , size_t const &s_lwork \
   ) { \
      int const m = static_cast<int const>(s_m); \
      int const n = static_cast<int const>(s_n); \
      int const k = static_cast<int const>(s_k); \
      int const lda = static_cast<int const>(s_lda); \
      int const ldc = static_cast<int const>(s_ldc); \
      int const lwork = static_cast<int const>(s_lwork); \
      int info = 0; \
      SOURCE(name)( \
                   &side, &trans, &m, &n, &k  \
                ,  CAST_ ## mem_type(a),  &lda \
                ,  CAST_ ## mem_type(tau) \
                ,  CAST_ ## mem_type(c),  &ldc \
                ,  CAST_ ## mem_type(work), &lwork \
                ,  &info); \
      LAPACK_ERROR(__FUNCTION__, "Unknown error", info); \
   }

BUILD_COMPLEX(unmqr)

#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                     char const &side \
                   , char const &trans \
                   , int const &m \
                   , int const &n \
                   , int const &k \
                   , TYPE_ ## mem_type(data_type) a \
                   , int const &lda \
                   , TYPE_ ## mem_type(data_type) tau \
                   , TYPE_ ## mem_type(data_type) c \
                   , int const &ldc \
                   , TYPE_ ## mem_type(data_type) work \
      ) { \
      int info = 0; \
      int const lwork = static_cast<int const>(work.size()); \
      SOURCE(name)( \
                   &side, &trans, &m, &n, &k  \
                ,  CAST_ ## mem_type(a),  &lda \
                ,  CAST_ ## mem_type(tau) \
                ,  CAST_ ## mem_type(c),  &ldc \
                ,  CAST_ ## mem_type(work), &lwork \
                ,  &info); \
      LAPACK_ERROR(__FUNCTION__, "Unknown error", info); \
   } \
   inline void base( \
                     char const &side \
                   , char const &trans \
                   , size_t const &s_m \
                   , size_t const &s_n \
                   , size_t const &s_k \
                   , TYPE_ ## mem_type(data_type) a \
                   , size_t const &s_lda \
                   , TYPE_ ## mem_type(data_type) tau \
                   , TYPE_ ## mem_type(data_type) c \
                   , size_t const &s_ldc \
                   , TYPE_ ## mem_type(data_type) work \
   ) { \
      int const m = static_cast<int const>(s_m); \
      int const n = static_cast<int const>(s_n); \
      int const k = static_cast<int const>(s_k); \
      int const lda = static_cast<int const>(s_lda); \
      int const ldc = static_cast<int const>(s_ldc); \
      int const lwork = static_cast<int const>(work.size()); \
      int info = 0; \
      SOURCE(name)( \
                   &side, &trans, &m, &n, &k  \
                ,  CAST_ ## mem_type(a),  &lda \
                ,  CAST_ ## mem_type(tau) \
                ,  CAST_ ## mem_type(c),  &ldc \
                ,  CAST_ ## mem_type(work), &lwork \
                ,  &info); \
      LAPACK_ERROR(__FUNCTION__, "Unknown error", info); \
   }

BUILD_COMPLEX_VECTOR(unmqr)


   /*
    * UNMRZ overwrites the general complex M-by-N matrix C with
    *
    *                 SIDE = 'L'     SIDE = 'R'
    * TRANS = 'N':      Q * C          C * Q
    * TRANS = 'C':      Q**H * C       C * Q**H
    *
    * where Q is a complex unitary matrix defined as the product of k
    * elementary reflectors
    *
    *       Q = H(1) H(2) . . . H(k)
    *
    * as returned by ZTZRZF. Q is of order M if SIDE = 'L' and of order N
    * if SIDE = 'R'.
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                     char const &side \
                   , char const &trans \
                   , int const &m \
                   , int const &n \
                   , int const &k \
                   , int const &l \
                   , TYPE_ ## mem_type(data_type) a \
                   , int const &lda \
                   , TYPE_ ## mem_type(data_type) tau \
                   , TYPE_ ## mem_type(data_type) c \
                   , int const &ldc \
                   , TYPE_ ## mem_type(data_type) work \
                   , int const &lwork \
      ) { \
      int info = 0; \
      SOURCE(name)( \
                   &side, &trans, &m, &n, &k, &l  \
                ,  CAST_ ## mem_type(a),  &lda \
                ,  CAST_ ## mem_type(tau) \
                ,  CAST_ ## mem_type(c),  &ldc \
                ,  CAST_ ## mem_type(work), &lwork \
                ,  &info); \
      LAPACK_ERROR(__FUNCTION__, "Unknown error", info); \
   } \
   inline void base( \
                     char const &side \
                   , char const &trans \
                   , size_t const &s_m \
                   , size_t const &s_n \
                   , size_t const &s_k \
                   , size_t const &s_l \
                   , TYPE_ ## mem_type(data_type) a \
                   , size_t const &s_lda \
                   , TYPE_ ## mem_type(data_type) tau \
                   , TYPE_ ## mem_type(data_type) c \
                   , size_t const &s_ldc \
                   , TYPE_ ## mem_type(data_type) work \
                   , size_t const &s_lwork \
   ) { \
      int const m = static_cast<int const>(s_m); \
      int const n = static_cast<int const>(s_n); \
      int const k = static_cast<int const>(s_k); \
      int const l = static_cast<int const>(s_l); \
      int const lda = static_cast<int const>(s_lda); \
      int const ldc = static_cast<int const>(s_ldc); \
      int const lwork = static_cast<int const>(s_lwork); \
      int info = 0; \
      SOURCE(name)( \
                   &side, &trans, &m, &n, &k, &l  \
                ,  CAST_ ## mem_type(a),  &lda \
                ,  CAST_ ## mem_type(tau) \
                ,  CAST_ ## mem_type(c),  &ldc \
                ,  CAST_ ## mem_type(work), &lwork \
                ,  &info); \
      LAPACK_ERROR(__FUNCTION__, "Unknown error", info); \
   }

BUILD_COMPLEX(unmrz)

#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                     char const &side \
                   , char const &trans \
                   , int const &m \
                   , int const &n \
                   , int const &k \
                   , int const &l \
                   , TYPE_ ## mem_type(data_type) a \
                   , int const &lda \
                   , TYPE_ ## mem_type(data_type) tau \
                   , TYPE_ ## mem_type(data_type) c \
                   , int const &ldc \
                   , TYPE_ ## mem_type(data_type) work \
      ) { \
      int info = 0; \
      int const lwork = static_cast<int const>(work.size()); \
      SOURCE(name)( \
                   &side, &trans, &m, &n, &k, &l  \
                ,  CAST_ ## mem_type(a),  &lda \
                ,  CAST_ ## mem_type(tau) \
                ,  CAST_ ## mem_type(c),  &ldc \
                ,  CAST_ ## mem_type(work), &lwork \
                ,  &info); \
      LAPACK_ERROR(__FUNCTION__, "Unknown error", info); \
   } \
   inline void base( \
                     char const &side \
                   , char const &trans \
                   , size_t const &s_m \
                   , size_t const &s_n \
                   , size_t const &s_k \
                   , size_t const &s_l \
                   , TYPE_ ## mem_type(data_type) a \
                   , size_t const &s_lda \
                   , TYPE_ ## mem_type(data_type) tau \
                   , TYPE_ ## mem_type(data_type) c \
                   , size_t const &s_ldc \
                   , TYPE_ ## mem_type(data_type) work \
   ) { \
      int const m = static_cast<int const>(s_m); \
      int const n = static_cast<int const>(s_n); \
      int const k = static_cast<int const>(s_k); \
      int const l = static_cast<int const>(s_l); \
      int const lda = static_cast<int const>(s_lda); \
      int const ldc = static_cast<int const>(s_ldc); \
      int const lwork = static_cast<int const>(work.size()); \
      int info = 0; \
      SOURCE(name)( \
                   &side, &trans, &m, &n, &k, &l  \
                ,  CAST_ ## mem_type(a),  &lda \
                ,  CAST_ ## mem_type(tau) \
                ,  CAST_ ## mem_type(c),  &ldc \
                ,  CAST_ ## mem_type(work), &lwork \
                ,  &info); \
      LAPACK_ERROR(__FUNCTION__, "Unknown error", info); \
   }

BUILD_COMPLEX_VECTOR(unmrz)


   /*
    *  TRTRS solves a triangular system of the form
    *
    *     A * X = B,  A**T * X = B,  or  A**H * X = B,
    *
    *  where A is a triangular matrix of order N, and B is an N-by-NRHS
    *  matrix.  A check is made to verify that A is nonsingular.
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                     char const &uplo \
                   , char const &trans \
                   , char const &diag\
                   , int const &n \
                   , int const &nrhs \
                   , TYPE_ ## mem_type(data_type) a \
                   , int const &lda \
                   , TYPE_ ## mem_type(data_type) b \
                   , int const &ldb \
      ) { \
      int info = 0; \
      SOURCE(name)( \
                   &uplo, &trans, &diag, &n, &nrhs \
                ,  CAST_ ## mem_type(a),  &lda \
                ,  CAST_ ## mem_type(b),  &ldb \
                ,  &info); \
      LAPACK_ERROR(__FUNCTION__, "Matrix is singular", info); \
   } \
   inline void base( \
                     char const &uplo \
                   , char const &trans \
                   , char const &diag \
                   , size_t const &s_n \
                   , size_t const &s_nrhs \
                   , TYPE_ ## mem_type(data_type) a \
                   , size_t const &s_lda \
                   , TYPE_ ## mem_type(data_type) b \
                   , size_t const &s_ldb \
   ) { \
      int const n = static_cast<int const>(s_n); \
      int const nrhs = static_cast<int const>(s_nrhs); \
      int const lda = static_cast<int const>(s_lda); \
      int const ldb = static_cast<int const>(s_ldb); \
      int info = 0; \
      SOURCE(name)( \
                   &uplo, &trans, &diag, &n, &nrhs  \
                ,  CAST_ ## mem_type(a),  &lda \
                ,  CAST_ ## mem_type(b),  &ldb \
                ,  &info); \
      LAPACK_ERROR(__FUNCTION__, "Matrix is singular", info); \
   }

BUILD_ALL(trtrs)


   /*
    * TRSM  solves one of the matrix equations
    *
    * op( A )*X = alpha*B,   or   X*op( A ) = alpha*B,
    *
    * where alpha is a scalar, X and B are m by n matrices, A is a unit, or
    * non-unit,  upper or lower triangular matrix  and  op( A )  is one  of
    *
    *    op( A ) = A   or   op( A ) = A**T   or   op( A ) = A**H.
    *
    * The matrix X is overwritten on B.
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                     char const &side \
                   , char const &uplo \
                   , char const &trans \
                   , char const &diag\
                   , int const &m \
                   , int const &n \
                   , data_type const &alpha \
                   , const TYPE_ ## mem_type(data_type) a \
                   , int const &lda \
                   , TYPE_ ## mem_type(data_type) b \
                   , int const &ldb \
      ) { \
      SOURCE(name)( \
                   &side, &uplo, &trans, &diag, &m, &n \
                ,  &alpha \
                ,  CAST_ ## mem_type(a),  &lda \
                ,  CAST_ ## mem_type(b),  &ldb \
            ); \
   } \
   inline void base( \
                     char const &side \
                   , char const &uplo \
                   , char const &trans \
                   , char const &diag \
                   , size_t const &s_m \
                   , size_t const &s_n \
                   , data_type const &alpha \
                   , const TYPE_ ## mem_type(data_type) a \
                   , size_t const &s_lda \
                   , TYPE_ ## mem_type(data_type) b \
                   , size_t const &s_ldb \
   ) { \
      int const m = static_cast<int const>(s_m); \
      int const n = static_cast<int const>(s_n); \
      int const lda = static_cast<int const>(s_lda); \
      int const ldb = static_cast<int const>(s_ldb); \
      SOURCE(name)( \
                   &side, &uplo, &trans, &diag, &m, &n \
                ,  &alpha \
                ,  CAST_ ## mem_type(a),  &lda \
                ,  CAST_ ## mem_type(b),  &ldb \
            ); \
   }

BUILD_ALL(trsm)


   /*
    * TZRZF reduces the M-by-N ( M<=N ) complex upper trapezoidal matrix A
    * to upper triangular form by means of unitary transformations.
    *
    * The upper trapezoidal matrix A is factored as
    *
    * A = ( R  0 ) * Z,
    *
    * where Z is an N-by-N unitary matrix and R is an M-by-M upper
    * triangular matrix.
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                     int const &m \
                   , int const &n \
                   , TYPE_ ## mem_type(data_type) a \
                   , int const &lda \
                   , TYPE_ ## mem_type(data_type) tau \
                   , TYPE_ ## mem_type(data_type) work \
                   , int const &lwork \
   ) { \
      int info = 0; \
      SOURCE(name)( \
                   &m, &n \
                 , CAST_ ## mem_type(a),  &lda \
                 , CAST_ ## mem_type(tau) \
                 , CAST_ ## mem_type(work),  &lwork \
                 , &info \
            ); \
      LAPACK_ERROR(__FUNCTION__, "Unknown error", info); \
   } \
   inline void base( \
                     size_t const &s_m \
                   , size_t const &s_n \
                   , TYPE_ ## mem_type(data_type) a \
                   , size_t const &s_lda \
                   , TYPE_ ## mem_type(data_type) tau \
                   , TYPE_ ## mem_type(data_type) work \
                   , size_t const &s_lwork \
   ) { \
      int const m = static_cast<int const>(s_m); \
      int const n = static_cast<int const>(s_n); \
      int const lda = static_cast<int const>(s_lda); \
      int const lwork = static_cast<int const>(s_lwork); \
      int info = 0; \
      SOURCE(name)( \
                   &m, &n \
                 , CAST_ ## mem_type(a),  &lda \
                 , CAST_ ## mem_type(tau) \
                 , CAST_ ## mem_type(work),  &lwork \
                 , &info \
            ); \
      LAPACK_ERROR(__FUNCTION__, "Unknown error", info); \
   }

BUILD_ALL(tzrzf)


   /*
    * GETRF computes an LU factorization of a general M-by-N matrix A
    * using partial pivoting with row interchanges.
    *
    * The factorization has the form
    *    A = P * L * U
    * where P is a permutation matrix, L is lower triangular with unit
    * diagonal elements (lower trapezoidal if m > n), and U is upper
    * triangular (upper trapezoidal if m < n).
    *
    * This is the Crout Level 3 BLAS version of the algorithm.
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                     int const &m \
                   , int const &n \
                   , TYPE_ ## mem_type(data_type) a \
                   , int const &lda \
                   , TYPE_ ## mem_type(int) ipiv \
   ) { \
      int info = 0; \
      SOURCE(name)( \
                   &m, &n \
                 , CAST_ ## mem_type(a),  &lda \
                 , CAST_ ## mem_type(ipiv) \
                 , &info \
            ); \
      LAPACK_ERROR(__FUNCTION__, "Unknown error", info); \
   } \
   inline void base( \
                     size_t const &s_m \
                   , size_t const &s_n \
                   , TYPE_ ## mem_type(data_type) a \
                   , size_t const &s_lda \
                   , TYPE_ ## mem_type(int) ipiv \
   ) { \
      int const m = static_cast<int const>(s_m); \
      int const n = static_cast<int const>(s_n); \
      int const lda = static_cast<int const>(s_lda); \
      int info = 0; \
      SOURCE(name)( \
                   &m, &n \
                 , CAST_ ## mem_type(a),  &lda \
                 , CAST_ ## mem_type(ipiv) \
                 , &info \
            ); \
      LAPACK_ERROR(__FUNCTION__, "U is singular", info); \
   }

BUILD_ALL(getrf)


   /*
    *  GETRI computes the inverse of a matrix using the LU factorization
    * computed by ZGETRF.
    *
    * This method inverts U and then computes inv(A) by solving the system
    * inv(A)*L = inv(U) for inv(A).
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                     int const &n \
                   , TYPE_ ## mem_type(data_type) a \
                   , int const &lda \
                   , TYPE_ ## mem_type(int) ipiv \
                   , TYPE_ ## mem_type(data_type) work \
                   , int const &lwork \
   ) { \
      int info = 0; \
      SOURCE(name)( \
                   &n \
                 , CAST_ ## mem_type(a),  &lda \
                 , CAST_ ## mem_type(ipiv) \
                 , CAST_ ## mem_type(work),  &lwork \
                 , &info \
            ); \
      LAPACK_ERROR(__FUNCTION__, "Unknown error", info); \
   } \
   inline void base( \
                     size_t const &s_n \
                   , TYPE_ ## mem_type(data_type) a \
                   , size_t const &s_lda \
                   , TYPE_ ## mem_type(int) ipiv \
                   , TYPE_ ## mem_type(data_type) work \
                   , size_t const &s_lwork \
   ) { \
      int const n = static_cast<int const>(s_n); \
      int const lda = static_cast<int const>(s_lda); \
      int const lwork = static_cast<int const>(s_lwork); \
      int info = 0; \
      SOURCE(name)( \
                   &n \
                 , CAST_ ## mem_type(a),  &lda \
                 , CAST_ ## mem_type(ipiv) \
                 , CAST_ ## mem_type(work),  &lwork \
                 , &info \
            ); \
      LAPACK_ERROR(__FUNCTION__, "U is singular", info); \
   }

BUILD_ALL(getri)


   /*
    * GETRS solves a system of linear equations
    *     A * X = B,  A**T * X = B,  or  A**H * X = B
    * with a general N-by-N matrix A using the LU factorization computed
    * by GETRF.
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                     char const &trans \
                   , int const &n \
                   , int const &nrhs \
                   , TYPE_ ## mem_type(data_type) a \
                   , int const &lda \
                   , TYPE_ ## mem_type(int) ipiv \
                   , TYPE_ ## mem_type(data_type) b \
                   , int const &ldb \
   ) { \
      int info = 0; \
      SOURCE(name)( \
                   &trans, &n, &nrhs \
                 , CAST_ ## mem_type(a),  &lda \
                 , CAST_ ## mem_type(ipiv) \
                 , CAST_ ## mem_type(b),  &ldb \
                 , &info \
            ); \
      LAPACK_ERROR(__FUNCTION__, "Unknown error", info); \
   } \
   inline void base( \
                     char const &trans \
                   , size_t const &s_n \
                   , size_t const &s_nrhs \
                   , TYPE_ ## mem_type(data_type) a \
                   , size_t const &s_lda \
                   , TYPE_ ## mem_type(int) ipiv \
                   , TYPE_ ## mem_type(data_type) b \
                   , size_t const &s_ldb \
   ) { \
      int const n = static_cast<int const>(s_n); \
      int const nrhs = static_cast<int const>(s_nrhs); \
      int const lda = static_cast<int const>(s_lda); \
      int const ldb = static_cast<int const>(s_ldb); \
      int info = 0; \
      SOURCE(name)( \
                   &trans, &n, &nrhs \
                 , CAST_ ## mem_type(a),  &lda \
                 , CAST_ ## mem_type(ipiv) \
                 , CAST_ ## mem_type(b),  &ldb \
                 , &info \
            ); \
      LAPACK_ERROR(__FUNCTION__, "Unknown error", info); \
   }

BUILD_ALL(getrs)


   /*
    *  POTRF computes the Cholesky factorization of a complex Hermitian
    *  positive definite matrix A.
    *
    *  The factorization has the form
    *     A = U**H * U,  if UPLO = 'U', or
    *     A = L  * L**H,  if UPLO = 'L',
    *  where U is an upper triangular matrix and L is lower triangular.
    *
    *  This is the block version of the algorithm, calling Level 3 BLAS.
    *
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                     char const &uplo \
                   , int const &n \
                   , TYPE_ ## mem_type(data_type) a \
                   , int const &lda \
   ) { \
      int info = 0; \
      SOURCE(name)( \
                   &uplo , &n \
                 , CAST_ ## mem_type(a),  &lda \
                 , &info \
            ); \
      LAPACK_ERROR(__FUNCTION__, "Diagonal is zero", info); \
   } \
   inline void base( \
                     char const &uplo \
                   , size_t const &s_n \
                   , TYPE_ ## mem_type(data_type) a \
                   , size_t const &s_lda \
   ) { \
      int const n = static_cast<int const>(s_n); \
      int const lda = static_cast<int const>(s_lda); \
      int info = 0; \
      SOURCE(name)( \
                   &uplo, &n \
                 , CAST_ ## mem_type(a),  &lda \
                 , &info \
            ); \
      LAPACK_ERROR(__FUNCTION__, "Diagonal is zero", info); \
   }

BUILD_ALL(potri)


   /*
    *  POTRI computes the inverse of a complex Hermitian positive definite
    *  matrix A using the Cholesky factorization A = U**H*U or A = L*L**H
    *  computed by POTRF.
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                     char const &uplo \
                   , int const &n \
                   , TYPE_ ## mem_type(data_type) a \
                   , int const &lda \
   ) { \
      int info = 0; \
      SOURCE(name)( \
                   &uplo , &n \
                 , CAST_ ## mem_type(a),  &lda \
                 , &info \
            ); \
      LAPACK_ERROR(__FUNCTION__, "Not positive definite", info); \
   } \
   inline void base( \
                     char const &uplo \
                   , size_t const &s_n \
                   , TYPE_ ## mem_type(data_type) a \
                   , size_t const &s_lda \
   ) { \
      int const n = static_cast<int const>(s_n); \
      int const lda = static_cast<int const>(s_lda); \
      int info = 0; \
      SOURCE(name)( \
                   &uplo, &n \
                 , CAST_ ## mem_type(a),  &lda \
                 , &info \
            ); \
      LAPACK_ERROR(__FUNCTION__, "Not positive definite", info); \
   }

BUILD_ALL(potrf)


   /*
    *  LASCL multiplies the M by N complex matrix A by the real scalar
    *  CTO/CFROM.  This is done without over/underflow as long as the final
    *  result CTO*A(I,J)/CFROM does not over/underflow. TYPE specifies that
    *  A may be full, upper triangular, lower triangular, upper Hessenberg,
    *  or banded.
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                     char const &type \
                   , int const &kl \
                   , int const &ku \
                   , base_type const &cfrom \
                   , base_type const &cto \
                   , int const &m \
                   , int const &n \
                   , TYPE_ ## mem_type(data_type) a \
                   , int const &lda \
   ) { \
      int info = 0; \
      SOURCE(name)( \
                   &type, &kl, &ku, &cfrom, &cto, &m, &n \
                 , CAST_ ## mem_type(a),  &lda \
                 , &info \
            ); \
      LAPACK_ERROR(__FUNCTION__, "Unkown error", info); \
   } \
   inline void base( \
                     char const &type \
                   , size_t const &s_kl \
                   , size_t const &s_ku \
                   , base_type const &cfrom \
                   , base_type const &cto \
                   , size_t const &s_m \
                   , size_t const &s_n \
                   , TYPE_ ## mem_type(data_type) a \
                   , size_t const &s_lda \
   ) { \
      int const kl = static_cast<int const>(s_kl); \
      int const ku = static_cast<int const>(s_ku); \
      int const m = static_cast<int const>(s_m); \
      int const n = static_cast<int const>(s_n); \
      int const lda = static_cast<int const>(s_lda); \
      int info = 0; \
      SOURCE(name)( \
                   &type, &kl, &ku, &cfrom, &cto, &m, &n \
                 , CAST_ ## mem_type(a),  &lda \
                 , &info \
            ); \
      LAPACK_ERROR(__FUNCTION__, "Unkown error", info); \
   }

BUILD_ALL(lascl)


   /* LAIC1 applies one step of incremental condition estimation in
    * its simplest version:
    *
    * Let x, twonorm(x) = 1, be an approximate singular vector of an j-by-j
    * lower triangular matrix L, such that
    *          twonorm(L*x) = sest
    * Then LAIC1 computes sestpr, s, c such that
    * the vector
    *                 [ s*x ]
    *          xhat = [  c  ]
    * is an approximate singular vector of
    *                 [ L       0  ]
    *          Lhat = [ w**H gamma ]
    * in the sense that
    *          twonorm(Lhat*xhat) = sestpr.
    *
    * Depending on JOB, an estimate for the largest or smallest singular
    * value is computed.
    *
    * Note that [s c]**H and sestpr**2 is an eigenpair of the system
    *
    *     diag(sest*sest, 0) + [alpha  gamma] * [ conjg(alpha) ]
    *                                           [ conjg(gamma) ]
    *
    * where  alpha =  x**H * w.
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                     int const &job \
                   , int const &j \
                   , const TYPE_ ## mem_type(data_type) x \
                   , base_type const &sest \
                   , const TYPE_ ## mem_type(data_type) w \
                   , data_type const &gamma \
                   , base_type &sestpr \
                   , data_type &s \
                   , data_type &c \
   ) { \
      SOURCE(name)( \
                   &job, &j \
                 , CAST_ ## mem_type(x) \
                 , &sest \
                 , CAST_ ## mem_type(w) \
                 , &gamma, &sestpr, &s, &c \
            ); \
   } \
   inline void base( \
                     int const &job \
                   , size_t const &s_j \
                   , const TYPE_ ## mem_type(data_type) x \
                   , base_type const &sest \
                   , const TYPE_ ## mem_type(data_type) w \
                   , data_type const &gamma \
                   , base_type &sestpr \
                   , data_type &s \
                   , data_type &c \
   ) { \
      int const j = static_cast<int const>(s_j); \
      SOURCE(name)( \
                   &job, &j \
                 , CAST_ ## mem_type(x) \
                 , &sest \
                 , CAST_ ## mem_type(w) \
                 , &gamma, &sestpr, &s, &c \
            ); \
   }

BUILD_ALL(laic1)


   /*
    *  HETRI computes the inverse of a complex Hermitian indefinite matrix
    *  A using the factorization A = U*D*U**H or A = L*D*L**H computed by
    *  HETRF.
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                     char const &uplo \
                   , int const &n \
                   , TYPE_ ## mem_type(data_type) a \
                   , int const &lda \
                   , TYPE_ ## mem_type(int) ipiv \
                   , TYPE_ ## mem_type(data_type) work \
   ) { \
      int info = 0; \
      SOURCE(name)( \
                   &uplo, &n \
                 , CAST_ ## mem_type(a), &lda \
                 , CAST_ ## mem_type(ipiv) \
                 , CAST_ ## mem_type(work) \
                 , &info \
            ); \
      LAPACK_ERROR(__FUNCTION__, "Matrix is singular", info); \
   } \
   inline void base( \
                     char const &uplo \
                   , size_t const &s_n \
                   , TYPE_ ## mem_type(data_type) a \
                   , size_t const &s_lda \
                   , TYPE_ ## mem_type(int) ipiv \
                   , TYPE_ ## mem_type(data_type) work \
   ) { \
      int const n = static_cast<int const>(s_n); \
      int const lda = static_cast<int const>(s_lda); \
      int info = 0; \
      SOURCE(name)( \
                   &uplo, &n \
                 , CAST_ ## mem_type(a), &lda \
                 , CAST_ ## mem_type(ipiv) \
                 , CAST_ ## mem_type(work) \
                 , &info \
            ); \
      LAPACK_ERROR(__FUNCTION__, "Matrix is singular", info); \
   }

BUILD_COMPLEX(hetri)


   /*
    *  HETRF computes the factorization of a complex Hermitian matrix A
    *  using the Bunch-Kaufman diagonal pivoting method.  The form of the
    *  factorization is
    *
    *     A = U*D*U**H  or  A = L*D*L**H
    *
    *  where U (or L) is a product of permutation and unit upper (lower)
    *  triangular matrices, and D is Hermitian and block diagonal with
    *  1-by-1 and 2-by-2 diagonal blocks.
    *
    *  This is the blocked version of the algorithm, calling Level 3 BLAS.
    */
#undef BUILD_FUNCTION
#define BUILD_FUNCTION(data_type, mem_type, base_type, name, base) \
   inline void base( \
                     char const &uplo \
                   , int const &n \
                   , TYPE_ ## mem_type(data_type) a \
                   , int const &lda \
                   , TYPE_ ## mem_type(int) ipiv \
                   , TYPE_ ## mem_type(data_type) work \
                   , int const &lwork \
   ) { \
      int info = 0; \
      SOURCE(name)( \
                   &uplo, &n \
                 , CAST_ ## mem_type(a), &lda \
                 , CAST_ ## mem_type(ipiv) \
                 , CAST_ ## mem_type(work) \
                 , &lwork \
                 , &info \
            ); \
      LAPACK_ERROR(__FUNCTION__, "Matrix is singular", info); \
   } \
   inline void base( \
                     char const &uplo \
                   , size_t const &s_n \
                   , TYPE_ ## mem_type(data_type) a \
                   , size_t const &s_lda \
                   , TYPE_ ## mem_type(int) ipiv \
                   , TYPE_ ## mem_type(data_type) work \
                   , size_t const &s_lwork \
   ) { \
      int const n = static_cast<int const>(s_n); \
      int const lda = static_cast<int const>(s_lda); \
      int const lwork = static_cast<int const>(s_lwork); \
      int info = 0; \
      SOURCE(name)( \
                   &uplo, &n \
                 , CAST_ ## mem_type(a), &lda \
                 , CAST_ ## mem_type(ipiv) \
                 , CAST_ ## mem_type(work) \
                 , &lwork \
                 , &info \
            ); \
      LAPACK_ERROR(__FUNCTION__, "Matrix is singular", info); \
   }

BUILD_COMPLEX(hetrf)


   /*
    *  HEFILL compute the missing half of complex Hermitian matrix A
    */
   template<typename T> void hefill(char const &uplo, size_t const &n, T *a)
   {
      if (uplo == 'U') {
         // Update the lower portion
         T *output;
         T *input(a);
         for (size_t i=0; i<n; i++) {
            size_t j(i+1);
            input += j;
            output = input+n-1;
            while (j<n) {
               *input = std::conj(*output);
               input++;
               output+=n;
               j++;
            }
         }
      }
      else {
         // Update the upper portion
         T *output;
         T *input(a);
         for (size_t i=0; i<n; i++) {
            size_t j(i+1);
            input += j;
            output = input+n-1;
            while (j<n) {
               *output = std::conj(*input);  // Note input/output reversed
               input++;
               output+=n;
               j++;
            }
         }
      }
   }
   template<typename T> void hefill(char const &uplo, size_t const &n, std::vector<T> &a)
   {
      hefill(uplo, n, &(a.front()));
   }

   /*
    *  HECPY computes the missing half of complex Hermitian matrix A
    *  and copy the results to C
    */
   template<typename T> void hecpy(char const &uplo, size_t const &n, T const *a, T *c)
   {
      if (uplo == 'U') {
         // Use upper portion of a
         T *output(c);
         T const *input;
         for (size_t i=0; i<n; i++) {
            input = a + i*n;

            size_t j(0);
            while (j <= i) {
               *output = *input;
               output++;
               input++;
               j++;
            }
            input+=n-1;
            while (j < n) {
               *output = std::conj(*input);
               output++;
               input+=n;
               j++;
            }
         }
      }
      else {
         // Use lower portion of a
         T *output(c);
         T const *input;
         for (size_t i=0; i<n; i++) {
            input = a + i;

            size_t j(0);
            while (j < i) {
               *output = std::conj(*input);
               output++;
               input+=n;
               j++;
            }
            while (j < n) {
               *output = *input;
               output++;
               input++;
               j++;
            }
         }
      }
   }
   template<typename T> void hecpy(char const &uplo, size_t const &n, std::vector<T> const &a, std::vector<T> &c)
   {
      hecpy(uplo, n, &(a.front()), &(c.front()));
   }
}

#endif
